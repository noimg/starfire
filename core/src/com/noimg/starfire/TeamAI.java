package com.noimg.starfire;

public class TeamAI {
	public final Level level;

	public enum Level {
		DUMB, MEDIUM, INTELLIGENT
	}

	public TeamAI (Level level) {
		this.level = level;
	}
}
