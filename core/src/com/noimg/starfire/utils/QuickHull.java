package com.noimg.starfire.utils;

//This is a java program to find a Vector2s in convex hull using quick hull method
//source: Alexander Hrishov's website
//URL: http://www.ahristov.com/tutorial/geometry-games/convex-hull.html

import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;

public class QuickHull {
	public ArrayList<Vector2> get(ArrayList<Vector2> points) {
		ArrayList<Vector2> convexHull = new ArrayList<Vector2>();
		if (points.size() < 3)
			return (ArrayList) points.clone();

		int minVector2 = -1, maxVector2 = -1;
		int minX = Integer.MAX_VALUE;
		int maxX = Integer.MIN_VALUE;
		for (int i = 0; i < points.size(); i++) {
			if (points.get(i).x < minX) {
				minX = (int) points.get(i).x;
				minVector2 = i;
			}
			if (points.get(i).x > maxX) {
				maxX = (int) points.get(i).x;
				maxVector2 = i;
			}
		}
		Vector2 A = points.get(minVector2);
		Vector2 B = points.get(maxVector2);
		convexHull.add(A);
		convexHull.add(B);
		points.remove(A);
		points.remove(B);

		ArrayList<Vector2> leftSet = new ArrayList<Vector2>();
		ArrayList<Vector2> rightSet = new ArrayList<Vector2>();

		for (int i = 0; i < points.size(); i++) {
			Vector2 p = points.get(i);
			if (Vector2Location(A, B, p) == -1)
				leftSet.add(p);
			else if (Vector2Location(A, B, p) == 1)
				rightSet.add(p);
		}
		hullSet(A, B, rightSet, convexHull);
		hullSet(B, A, leftSet, convexHull);

		return convexHull;
	}

	public int distance(Vector2 A, Vector2 B, Vector2 C) {
		int ABx = (int) (B.x - A.x);
		int ABy = (int) (B.y - A.y);
		int num = (int) (ABx * (A.y - C.y) - ABy * (A.x - C.x));
		if (num < 0)
			num = -num;
		return num * 2;
	}

	public void hullSet(Vector2 A, Vector2 B, ArrayList<Vector2> set, ArrayList<Vector2> hull) {
		int insertPosition = hull.indexOf(B);
		if (set.size() == 0)
			return;
		if (set.size() == 1) {
			Vector2 p = set.get(0);
			set.remove(p);
			hull.add(insertPosition, p);
			return;
		}
		int dist = Integer.MIN_VALUE;
		int furthestVector2 = -1;
		for (int i = 0; i < set.size(); i++) {
			Vector2 p = set.get(i);
			int distance = distance(A, B, p);
			if (distance > dist) {
				dist = distance;
				furthestVector2 = i;
			}
		}
		Vector2 P = set.get(furthestVector2);
		set.remove(furthestVector2);
		hull.add(insertPosition, P);

		// Determine who's to the left of AP
		ArrayList<Vector2> leftSetAP = new ArrayList<Vector2>();
		for (int i = 0; i < set.size(); i++) {
			Vector2 M = set.get(i);
			if (Vector2Location(A, P, M) == 1) {
				leftSetAP.add(M);
			}
		}

		// Determine who's to the left of PB
		ArrayList<Vector2> leftSetPB = new ArrayList<Vector2>();
		for (int i = 0; i < set.size(); i++) {
			Vector2 M = set.get(i);
			if (Vector2Location(P, B, M) == 1) {
				leftSetPB.add(M);
			}
		}

		hullSet(A, P, leftSetAP, hull);
		hullSet(P, B, leftSetPB, hull);
	}

	public int Vector2Location(Vector2 A, Vector2 B, Vector2 P) {
		int cp1 = (int) ((B.x - A.x) * (P.y - A.y) - (B.y - A.y) * (P.x - A.x));
		if (cp1 > 0)
			return 1;
		else if (cp1 == 0)
			return 0;
		else
			return -1;
	}
}