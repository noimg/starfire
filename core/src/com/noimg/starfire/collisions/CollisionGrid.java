package com.noimg.starfire.collisions;

import com.badlogic.gdx.graphics.Color;
import com.noimg.starfire.Starfire;

import java.util.ArrayList;
import java.util.List;

/**
 * Реализация алгоритма, который использует деление пространства на сетку для нахождения (и обработки)
 * столковений между объектами
 */

public class CollisionGrid {
	public final CollisionGridThread[] threads;
	private final CollisionGridCell[][] cells;
	private final int width, height;
	private int nextThread;

	public CollisionGrid(int width, int height) {
		this.width = width;
		this.height = height;
		cells = new CollisionGridCell[width][height];

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				cells[x][y] = new CollisionGridCell(x, y);
			}
		}

		int processors = Runtime.getRuntime().availableProcessors();
		threads = new CollisionGridThread[processors + 1];
	}

	public void initThreading() {
		for (int i = 0; i < threads.length; i++) {
			threads[i] = new CollisionGridThread();
			threads[i].start();

			switch (i) {
				case 0:
					threads[i].color = Color.WHITE;
					break;
				case 1:
					threads[i].color = Color.RED;
					break;
				case 2:
					threads[i].color = Color.BLUE;
					break;
				case 3:
					threads[i].color = Color.BROWN;
					break;
				case 4:
					threads[i].color = Color.CHARTREUSE;
					break;
				case 5:
					threads[i].color = Color.CORAL;
					break;
				case 6:
					threads[i].color = Color.GREEN;
					break;
				case 7:
					threads[i].color = Color.YELLOW;
					break;
				case 8:
					threads[i].color = Color.GRAY;
					break;
			}
		}

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				nextThread = (nextThread < threads.length - 1) ? nextThread + 1 : 0;
				cells[x][y].thread = threads[nextThread];
			}

			nextThread = x > threads.length ? x % threads.length : x;
		}
	}

	public GameObject[] getGameObjectsInRange(float x, float y, int range) {
		if (x >= 0 && y >= 0 && x < width * Starfire.COLLISION_CELL_SIZE && y < height * Starfire.COLLISION_CELL_SIZE) {
			List<GameObject> gameObjects = new ArrayList<GameObject>();
			for (CollisionGridCell cell : getCellsInRange((int)x, (int)y, range)) {
				gameObjects.addAll(cell.gameObjects);
			}

			return gameObjects.toArray(new GameObject[gameObjects.size()]);
		}

		return null;
	}

	/*
	public List<GameObject> getNearestGameObjects(float x, float y) {
		if (x >= 0 && y >= 0 && x < width * Starfire.COLLISION_CELL_SIZE && y < height * Starfire.COLLISION_CELL_SIZE) {
			int cellX = (int) x / Starfire.COLLISION_CELL_SIZE;
			int cellY = (int) y / Starfire.COLLISION_CELL_SIZE;
			return cells[cellX][cellY].gameObjects;
		}

		return null;
	}*/

	public CollisionGridCell[] getRectangleCells(int x, int y, int width, int height) {
		int startX = x / Starfire.COLLISION_CELL_SIZE;
		int endX = (x + width) / Starfire.COLLISION_CELL_SIZE;
		int startY = y / Starfire.COLLISION_CELL_SIZE;
		int endY = (y + height) / Starfire.COLLISION_CELL_SIZE;

		CollisionGridCell[] overlapCells = new CollisionGridCell[(endX - startX + 1) * (endY - startY + 1)];
		int i = 0;
		for (int xx = startX; xx <= endX; xx++) {
			for (int yy = startY; yy <= endY; yy++) {
				if (xx >= 0 && yy >= 0 && xx < this.width && yy < this.height) {
					overlapCells[i] = cells[xx][yy];
				}

				i++;
			}
		}

		return overlapCells;
	}

	public CollisionGridCell[] getCellsInRange (int centerX, int centerY, int range) {
		int diameter = range * 2;
		int x = centerX - range;
		int y = centerY - range;
		return getRectangleCells(x, y, diameter, diameter);
	}

	public CollisionGridCell[] getGameObjectCells(GameObject gameObject) {
		return getCellsInRange((int)gameObject.position.x, (int)gameObject.position.y, gameObject.radius);
	}

	/*
	private CollisionGridThread getOptimalThread () {
		int min = 1000;

		CollisionGridThread minThread = null;
		for (CollisionGridThread thread : threads) {
			int size = thread.units.size();
			if (size < min) {
				min = size;
				minThread = thread;
			}
		}

		return minThread;
	}*/

	public void insert(GameObject gameObject) {
		for (CollisionGridCell cell : getGameObjectCells(gameObject)) {
			if (cell == null) {
				System.out.println("Map out of bounds");
				continue;
			}

			cell.gameObjects.add(gameObject);

			if (cell.gameObjects.size() > 1) {
				cell.thread.cells.add(cell);
			}
		}
	}

	public void remove(GameObject gameObject) {
		for (CollisionGridCell cell : getGameObjectCells(gameObject)) {
			if (cell == null) {
				System.out.println("Map out of bounds");
				continue;
			}

			cell.gameObjects.remove(gameObject);
			cell.thread.cells.remove(cell);
		}
	}

	public boolean areAllThreadsFree() {
		for (CollisionGridThread thread : threads) {
			if (!thread.isFree) {
				return false;
			}
		}

		return true;
	}

	public void restartThreads () {
		for (CollisionGridThread thread : threads) {
			thread.isFree = false;
		}
	}

	public void update(float delta) {
		if (!areAllThreadsFree()) return;
		// Если не все треды свободны, updateThreaded в Starfire.game.world может задерживаться,
		// что визуально выглядит как фризы

		//todo Заставлять треды обрабатывать game.updateThreaded
		Starfire.game.world.updateThreaded(delta, threads);
		restartThreads();
	}

	public class CollisionGridThread extends Thread {
		public final List<CollisionGridCell> cells;
		public boolean isFree;
		public Color color;

		public CollisionGridThread() {
			cells = new ArrayList<CollisionGridCell>();
			setName("Collision Grid Thread");
		}

		@Override
		public void run() {
			while (!Thread.currentThread().isInterrupted()) {
				update ();

				try {
					Thread.sleep(33);
				} catch (InterruptedException e) {
					System.out.println("Interrupted Exception in " + getName());
				}
			}
		}

		public void update () {
			if (!isFree) {
				for (CollisionGridCell cell : cells) {
					cell.update();
				}

				isFree = true;
			}
		}
	}
}
