package com.noimg.starfire.units;

import com.badlogic.gdx.audio.Sound;
import com.noimg.starfire.world.Explosion;

/**
 * GameScreen обрабатывает DeathEvent'ы юнитов после их уничтожения для того, что бы воспроизвести определенные действия
 */

public class DeathEvent {
	public Explosion.Power explosion;
	public Sound sound;
	public CustomDeathEvent customDeathEvent;

	// Нет конструктора, т.к. проще включать фичи по отдельности

	public interface CustomDeathEvent {
		void event(float deathX, float deathY);
	}
}
