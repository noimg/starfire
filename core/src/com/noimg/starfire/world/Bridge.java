package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

public class Bridge {
	public final Star a, b;
	private final ShapeRenderer shaper;

	public Bridge(ShapeRenderer shaper, Star a, Star b) {
		this.shaper = shaper;
		this.a = a;
		this.b = b;
	}

	public void draw() {

	}
}
