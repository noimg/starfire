package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Assets;
import com.noimg.starfire.collisions.GameObject;
import com.noimg.starfire.units.Unit;

import java.util.List;

public class Wormhole extends Structure {
	private Wormhole target;

	public Wormhole(Star star, Wormhole target) {
		this(star);
		this.target = target;
	}

	public Wormhole(Star star) {
		super(star, 6000, 10f);
	}

	private boolean hasTarget() {
		return target != null;
	}

	public void update(float delta) {
		super.update(delta);
	}

	public void draw(SpriteBatch batch) {
		if (!builded) batch.setColor(1, 1, 1, 0.1f);
		batch.draw(Assets.Textures.HOLE, position.x - 350, position.y - 350);
		super.draw(batch);
	}

	private void teleport(Unit unit) {
		if (!hasTarget()) return;
		Vector2 teleportPosition = new Vector2(unit.position.x - position.x + target.position.x, unit.position.y - position.y + target.position.y);
		unit.position.set(teleportPosition);
	}

	private boolean inHole(Vector2 target) {
		return target.dst2(position) < radiusSquared;
	}

	@Override
	public void updateCollision(com.noimg.starfire.collisions.GameObject nearestGameObject) {
		if (!(nearestGameObject instanceof Unit)) return;
		Unit unit = (Unit) nearestGameObject;

		if (inHole(unit.position)) {
			teleport(unit);
		}
	}

	@Override
	public void destroy() {

	}
}
