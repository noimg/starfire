package com.noimg.starfire.units;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;
import com.noimg.starfire.world.Explosion;

public class ArmoredShip extends Unit {

	public ArmoredShip(float x, float y, Squad team) {
		super(x, y, team);
	}

	@Override
	public int getDefaultHealth() {
		return Card.ARMORED_SHIP.getIntProperty("health");
	}

	@Override
	public int getDefaultDamage() {
		return Card.ARMORED_SHIP.getIntProperty("damage");
	}

	@Override
	public float getDefaultSpeed() {
		return Card.ARMORED_SHIP.getIntProperty("speed");
	}

	@Override
	public int getDefaultRange() {
		return 270;
	}

	@Override
	public float getDefaultReloadTime() {
		return 0.3f;
	}

	@Override
	public Sound getShootSound() {
		return Assets.Sounds.SHOOT_2;
	}

	@Override
	public DeathEvent getDeathEvent() {
		DeathEvent event = new DeathEvent();
		event.explosion = Explosion.Power.HUGE;
		event.sound = Assets.Sounds.BIG_SHIP_DESTROY;
		return event;
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.ARMORED_SHIP;
	}
}
