package com.noimg.starfire;

import com.badlogic.gdx.graphics.Color;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.world.Star;

import java.util.ArrayList;
import java.util.List;

public enum Team {
	RED(Assets.Colors.RED),
	GREEN(Assets.Colors.GREEN),
	BLUE(Assets.Colors.BLUE),
	YELLOW(Assets.Colors.YELLOW),
	MAGENTA(Assets.Colors.MAGENTA),
	NEUTRAL(Assets.Colors.WHITE);

	public final Color color;
	public final List<Star> stars;
	public final List<Unit> units;
	public final List<Squad> squads;
	public final Card[] cards = new Card[Starfire.CARDS_IN_DECK];    // Список доступных карт
	public CardDeck deck;
	private int resources;

	Team(Color color) {
		this.color = color;
		stars = new ArrayList<Star>();
		units = new ArrayList<Unit>();
		squads = new ArrayList<Squad>();
	}

	public void addResources(int amount) {
		resources += amount;

		if (resources > 10) resources = 10;
		else if (this == Starfire.game.playerTeam) Starfire.game.ui.updateResourceTabs();
	}

	public void subResources(int amount) {
		resources -= amount;
		if (resources < 0) resources = 0;
		else if (this == Starfire.game.playerTeam) Starfire.game.ui.updateResourceTabs();
	}

	public int getResources() {
		return resources;
	}
}
