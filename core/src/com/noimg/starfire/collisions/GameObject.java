package com.noimg.starfire.collisions;

import com.badlogic.gdx.math.Vector2;

import java.util.List;

/**
 * GameObject в основном абстрагирует работу со столкновениями для более простого использования
 * и уменьшения колличества кода
 */

public abstract class GameObject {
	public final Vector2 position;
	public final int radius;
	public final int radiusSquared;
	public boolean collisionReady;
	public boolean dead;

	public GameObject(float x, float y, int radius) {
		this.position = new Vector2(x, y);
		this.radius = radius;
		this.radiusSquared = radius * radius;
	}


	public abstract void updateCollision(GameObject nearestGameObject);

	// Вызывается после того, как потоки были обработаны если флаг dead равен true
	public abstract void destroy ();
}
