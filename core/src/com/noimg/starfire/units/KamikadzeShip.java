package com.noimg.starfire.units;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;
import com.noimg.starfire.world.Explosion;

public class KamikadzeShip extends Unit {

	public KamikadzeShip(float x, float y, Squad team) {
		super(x, y, team);
	}

	@Override
	public int getDefaultHealth() {
		return Card.KAMIKADZE_SHIP.getIntProperty("health");
	}

	@Override
	public int getDefaultDamage() {
		return 0;
	}

	@Override
	public float getDefaultSpeed() {
		return Card.KAMIKADZE_SHIP.getIntProperty("speed");
	}

	@Override
	public int getDefaultRange() {
		return 80;
	}

	@Override
	public float getDefaultReloadTime() {
		return 0f;
	}

	@Override
	public Sound getShootSound() {
		return Assets.Sounds.SHOOT_2;
	}

	@Override
	public DeathEvent getDeathEvent() {
		DeathEvent event = new DeathEvent();
		event.explosion = (Explosion.Power) Card.KAMIKADZE_SHIP.getProperty("explosionpower");
		event.sound = Assets.Sounds.BIG_SHIP_DESTROY;
		return event;
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.ASSAULT_SHIP;
	}
}
