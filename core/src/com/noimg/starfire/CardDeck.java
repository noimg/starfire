package com.noimg.starfire;

public class CardDeck {
	public final Card[] cards = new Card[4];
	private final Team team;
	public int selected = -1;
	private int nextCard;

	public CardDeck(Team team) {
		this.team = team;

		for (int i = 0; i < 4; i++) {
			nextCard();
		}
	}

	private void addCard(Card card) {
		for (int i = 0; i < 4; i++) {
			if (cards[i] == null) {
				cards[i] = card;

				if (team == Starfire.game.playerTeam)
					Starfire.game.ui.addCard(card, i);

				return;
			}
		}

	}

	public void nextCard() {
		if (nextCard >= Starfire.CARDS_IN_DECK) {
			System.out.println("end of the deck");
			return;
		}

		addCard(team.cards[nextCard]);
		nextCard++;
	}

	/*
	public void removeCard (Card card) {
		for (int i = 0; i < 4; i++) {
			if (cards[i] == card) {
				cards[i] = null;
				return;
			}
		}
	}*/

	public Card getSelectedCard() {
		return cards[selected];
	}

	public void removeSelectedCard() {
		cards[selected] = null;
	}

	public boolean isCurrentCardIsLast() {
		return nextCard == Starfire.CARDS_IN_DECK;
	}
}
