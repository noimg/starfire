package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Starfire;

public class Wall {
	public final Vector2 a, b;

	private Wall (Vector2 a, Vector2 b) {
		this.a = a;
		this.b = b;
	}

	public static Wall newWall (Vector2 a, Vector2 b) {
		Wall wall = new Wall(a, b);
		Starfire.game.world.walls.add(wall);
		return wall;
	}

	public void draw (ShapeRenderer batch, float delta) {
		batch.line(a.x, a.y, b.x, b.y);
	}
}
