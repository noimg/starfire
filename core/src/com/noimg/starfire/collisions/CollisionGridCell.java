package com.noimg.starfire.collisions;

import java.util.ArrayList;
import java.util.List;

/**
 * Клетка в сетке коллизий. Содержит в себе список юнитов, которые в ней находятся и прямоугольник, обозначающий ее границы.
 * Вот и все. Да.
 */
public class CollisionGridCell {
	public final List<GameObject> gameObjects = new ArrayList<GameObject>();
	public final int x, y;    // вообще x y сейчас используется только в целях дебага, поэтому мб
	// в дальнейшем это можно будет удалить
	public com.noimg.starfire.collisions.CollisionGrid.CollisionGridThread thread;

	public CollisionGridCell(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public void update() {
		for (GameObject a : gameObjects) {
			//if (a.collisionReady) continue;

			for (GameObject b : gameObjects) {
				a.updateCollision (b);
				a.collisionReady = true;
			}
		}
	}
}
