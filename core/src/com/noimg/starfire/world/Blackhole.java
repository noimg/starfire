package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.units.Unit;

public class Blackhole extends Structure {
	private final int power;
	private final float lifetime;
	// Объявляем переменные, которые просто хранят в себе квадраты некоторых значений в целях оптимизации
	// поиска расстояния (dst2 в Vector2 не использует квадратный корень)
	private final float deathAreaRadiusSquared;
	private boolean activated;
	private float timer;
	private boolean soundPlayed;

	/*
	// todo Blackhole.Power enum
	public enum Power {

	}*/

	private Blackhole(Star star, int power, float lifetime, float deployTime) {
		super(star, 900, deployTime);
		this.power = power;
		this.lifetime = lifetime;
		float radius = 350;
		deathAreaRadiusSquared = radius * radius;
	}

	public static Blackhole newBlackhole (Star star, int power, float lifetime, float deployTime) {
		System.out.println(Starfire.game.world.collisionGrid.areAllThreadsFree());
		System.out.println("Тут может быть баг связанный с тем, что newBlackhole вызывается не " +
				"thread-safe. Фиксится реализацией очереди подобных событий. Да и создание юнитов тоже не " +
				"thread-safe. Запили очередь епты");

		Blackhole blackhole = new Blackhole(star, power, lifetime, deployTime);
		Starfire.game.world.blackholes.add(blackhole);
		Starfire.game.world.collisionGrid.insert(blackhole);

		return blackhole;
	}

	private void attract(Unit unit) {
		unit.addPointForce(position, (power * 25) * (timer / lifetime));
	}

	public void update(float delta) {
		super.update(delta);

		activated = builded;
		if (builded) {
			if (!soundPlayed) {
				Assets.Sounds.BLACK_HOLE.play();
				soundPlayed = true;
			}

			timer += delta;
			if (timer > lifetime) {
				timer = 0;
				dead = true;
			}
		}
	}

	public void draw(SpriteBatch batch) {
		if (!builded) batch.setColor(1, 1, 1, 0.1f);
		batch.draw(Assets.Textures.HOLE, position.x - 350, position.y - 350);
		if (activated)
			radialSprite.draw(batch, position.x - 350, position.y - 350, 700, 700, 360f * (1f - timer / lifetime));
		super.draw(batch);
	}


	private boolean inDeathArea(Vector2 target) {
		return target.dst2(position) < deathAreaRadiusSquared;
	}

	private boolean inRange(Vector2 target) {
		return target.dst2(position) < radiusSquared;
	}

	@Override
	public void updateCollision(com.noimg.starfire.collisions.GameObject nearestGameObject) {
		if (!(nearestGameObject instanceof Unit)) return;
		Unit unit = (Unit) nearestGameObject;

		if (inRange(unit.position)) {
			if (activated) {
				if (inDeathArea(unit.position)) {
					unit.dead = true;
					return;
				}

				attract(unit);
			}
		}
	}

	@Override
	public void destroy () {
		star.structures.remove(this);
		Starfire.game.world.blackholes.remove(this);
		Explosion.newExplosion(position.x, position.y, Explosion.Power.BIG, true);
	}
}
