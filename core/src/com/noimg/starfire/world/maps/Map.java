package com.noimg.starfire.world.maps;

public interface Map {
	void generate ();
	int getWidth ();
	int getHeight ();
}
