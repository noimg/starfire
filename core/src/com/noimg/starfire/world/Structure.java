package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.noimg.starfire.Assets;
import com.noimg.starfire.utils.RadialSprite;

public abstract class Structure extends com.noimg.starfire.collisions.GameObject {
	public final Star star;
	protected final RadialSprite radialSprite;
	private final float deployTime;
	protected boolean builded;
	private float deployTimer;

	Structure(Star star, int size, float deployTime) {
		super(star.position.x, star.position.y, size);
		this.star = star;
		this.deployTime = deployTime;
		radialSprite = new RadialSprite(Assets.Textures.RADIAL_PROGRESS_BAR);    // сюда нужно вставлять маску для каждого строения
		radialSprite.setColor(new Color(star.getTeam().color.r, star.getTeam().color.g, star.getTeam().color.b, 0.2f));
	}

	private void build(float delta) {
		deployTimer += delta;

		if (deployTimer >= deployTime) {
			deployTimer = deployTime;
			builded = true;
		}
	}

	void update(float delta) {
		if (!builded) {
			build(delta);
		}
	}

	void draw(SpriteBatch batch) {
		// fixme переделать метод, что бы он работал не только под окружности с радиусом 700
		if (builded) return;
		radialSprite.draw(batch, position.x - 350, position.y - 350, 700, 700, 360f * (1f - deployTimer / deployTime));
	}
}
