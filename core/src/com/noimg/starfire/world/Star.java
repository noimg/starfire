package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;
import com.noimg.starfire.Popup;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.collisions.GameObject;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.utils.RadialSprite;

import java.util.ArrayList;
import java.util.List;

public class Star extends com.noimg.starfire.collisions.GameObject {

	public final List<Structure> structures = new ArrayList<Structure>();
	// Лист звезд, соединенных с этой звездой мостами
	public final List<Star> neighbors = new ArrayList<Star>();
	private final List<Squad> deploySquadQueue = new ArrayList<Squad>();
	//private final String name;
	private final RadialSprite unitFactoryRadialSprite;
	private final RadialSprite starRadialSprite;
	private float resourceTimer;
	private float squadDeployTimer;
	private boolean isDeployingSquad;
	private Team team;
	private Team conquerTeam;
	private float conquerRate = 1f;
	private boolean isConquering;

	private Star(float x, float y, Team team) {
		super(x, y, 420);
		unitFactoryRadialSprite = new RadialSprite(Assets.Textures.STAR_SUN_MASK);
		starRadialSprite = new RadialSprite(Assets.Textures.RADIAL_PROGRESS_BAR);
		setTeam(team);
	}

	public static Star newStar(float x, float y, Team team) {
		Star star = new Star(x, y, team);
		Starfire.game.world.collisionGrid.insert(star);
		Starfire.game.world.stars.add(star);
		return star;
	}

	/*
	public final String getStarName (int number) {
		String name;
		int remainder;
		if (number >= 24) {
			remainder = number / 24;
			name = Assets.Misc.STAR_NAMES[number - remainder * 24] + " " + remainder;
		} else {
			name = Assets.Misc.STAR_NAMES[number];
		}

		return name;
	}*/

	private void conquer(Unit unit, float amount) {
		if (unit == null) return;    // затычка
		// Юнит уменьшает уровень захвата, так как звезда захвачена другой стороной
		if (unit.team != team) {
			conquerRate -= amount;
		}
		// Юнит увеличивает уровень захвата, так как звезда захвачена его стороной
		else {
			conquerRate += amount;
		}

		if (conquerRate > 1) {
			conquerRate = 1;
		} else if (conquerRate < 0) {
			conquerRate = 0.3f;

			if (this.team == Starfire.game.playerTeam) {
				Starfire.game.ui.addNotification("You lost star!", 2.5f);
			}

			setTeam(conquerTeam);
			isConquering = false;
		}

		if (unit.team != team && !isConquering) {
			conquerTeam = unit.team;
			isConquering = true;
		}
	}

	public boolean isFarStar() {
		return neighbors.size() == 1;
	}

	public void giveResource() {
		resourceTimer = 0;
		team.addResources(1);
	}

	public boolean canGiveResource(float delta) {
		resourceTimer += delta;
		return resourceTimer > Starfire.RESOURCE_STAR_GET_TIME;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		if (team == this.team) return;

		this.team = team;
		team.stars.add(this);
		unitFactoryRadialSprite.setColor(team.color);
		starRadialSprite.setColor(team.color);

		for (Star star : Starfire.game.world.stars) {
			Color color;

			if (star.getTeam() != Starfire.game.playerTeam && !star.hasSideNeighbors(Starfire.game.playerTeam)) {
				color = new Color(star.getTeam().color.r / 2, star.getTeam().color.g / 2, star.getTeam().color.b / 2, 1f);
			} else {
				color = star.getTeam().color;
			}

			star.starRadialSprite.setColor(color);
			star.unitFactoryRadialSprite.setColor(color);
		}
	}

	private boolean canDeploySquad(float delta) {
		squadDeployTimer += delta;
		return squadDeployTimer > getLastSquadInQueue().card.getFloatProperty("deploytime");
	}

	private Squad getLastSquadInQueue() {
		return deploySquadQueue.get(0);
	}

	private void deployLastSquadInQueue() {
		Starfire.game.world.squads.add(deploySquadQueue.get(0));
		stopSquadDeploy();
	}

	public void addSquadToDeployQueue(Squad deploySquad) {
		isDeployingSquad = true;
		deploySquadQueue.add(deploySquad);

		// Если отряд первый в очереди
		if (deploySquadQueue.get(0) == deploySquad) {
			squadDeployTimer = 0;
		}
	}

	private void stopSquadDeploy() {
		final float DEPLOY_TIME_FOR_NOTIFICATION = 5f;
		if (getLastSquadInQueue().card.getFloatProperty("deploytime") >= DEPLOY_TIME_FOR_NOTIFICATION)
			Starfire.game.ui.addNotification(getLastSquadInQueue().card.name + " just finished deploying", 2.5f);

		deploySquadQueue.remove(getLastSquadInQueue());

		if (deploySquadQueue.isEmpty()) {
			isDeployingSquad = false;
		}

		squadDeployTimer = 0;
	}

	public void update(float delta) {
		if (isDeployingSquad) {
			if (canDeploySquad(delta)) {
				deployLastSquadInQueue();
			}
		}


		if (canGiveResource(delta)) {
			giveResource();
			// Если звезда подконтрольна игроку
			if (getTeam() == Starfire.game.playerTeam) {
				// То добавляем попап
				Starfire.game.world.popups.add(new Popup(position, "+1R", 1, Starfire.game.playerTeam.color));
			}
		}
	}

	public void draw(SpriteBatch batch, float zoom) {
		if (!structures.isEmpty()) return;
		batch.setColor(team.color);
		//batch.draw (Assets.Textures.STAR, position.x - 350, position.y - 350, 700, 700);
		starRadialSprite.draw(batch, position.x - 350, position.y - 350, 700, 700, 360f * (1f - conquerRate));

		//fixme optimize

		//if (addSquadToDeployQueue != null)
		//	rate = 1f - (squadDeployTimer + squadDeployTime * nextUnit) / (squadDeployTime * addSquadToDeployQueue.units.size());

		if (isDeployingSquad) {
			float rate = 1f - (squadDeployTimer) / (getLastSquadInQueue().card.getFloatProperty("deploytime"));
			Texture cardTexture = getLastSquadInQueue().card.texture;
			unitFactoryRadialSprite.draw(batch, position.x - 186, position.y - 186, 372, 372, 360 * rate);
			batch.setColor(new Color(team.color.r / 2, team.color.g / 2, team.color.b / 2, 1));
			batch.draw(cardTexture, position.x - cardTexture.getWidth() / 2, position.y - cardTexture.getHeight() / 2);
		}
	}

	public boolean hasSideNeighbors(Team team) {
		for (Star neighbor : neighbors) {
			if (neighbor.getTeam() == team) return true;
		}

		return false;
	}

	public void buildBridge(Star star) {
		neighbors.add(star);
		star.neighbors.add(this);
	}

	public void destroyBridge(Star star) {
		neighbors.remove(star);
		star.neighbors.remove(star);
	}

	public boolean inRange(Vector2 target) {
		return target.dst2(position) < radiusSquared;
	}

	@Override
	public void updateCollision(GameObject nearestGameObject) {
		if (!(nearestGameObject instanceof Unit)) return;
		Unit unit = (Unit) nearestGameObject;

		conquer(unit, 0.00001f);
	}

	@Override
	public void destroy() {

	}
}
