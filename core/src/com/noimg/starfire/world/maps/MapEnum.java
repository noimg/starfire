package com.noimg.starfire.world.maps;

import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Team;
import com.noimg.starfire.world.Star;
import com.noimg.starfire.world.Wall;

public enum MapEnum implements Map {
	HEXAGON {
		@Override
		public void generate() {
			Star.newStar(2000, 2000, Team.RED);
			Star.newStar(10000, 2000, Team.BLUE);
			Wall.newWall(new Vector2(6000, 1000), new Vector2(6000, 3000));
		}

		@Override
		public int getWidth() {
			return 12000;
		}

		@Override
		public int getHeight() {
			return 4000;
		}
	}
}
