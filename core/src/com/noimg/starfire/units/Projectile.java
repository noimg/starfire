package com.noimg.starfire.units;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.noimg.starfire.Starfire;

public class Projectile {

	private final Unit shooter;
	private final Unit target;
	public boolean disappear;
	private float timer;
	private Color color;

	private Projectile(Unit shooter, Unit target) {
		this.shooter = shooter;
		this.target = target;
		color = shooter.team.color.cpy();
	}

	public static Projectile newProjectile(Unit shooter, Unit target) {
		Projectile projectile = new Projectile(shooter, target);
		Starfire.game.threadSound = projectile.shooter.getShootSound();
		return projectile;
	}

	public void draw(float delta, ShapeRenderer shaper) {
		timer += delta;

		if (timer > 1f) {
			color.a -= 0.07f;
		}

		if (shooter.dead || target.dead || timer > 1.5f || shooter.position.dst2(target.position) > shooter.radiusSquared) {
			disappear = true;
		}

		shaper.setColor(color);
		shaper.line(shooter.position.x, shooter.position.y, target.position.x, target.position.y);
	}
}
