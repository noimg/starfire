package com.noimg.starfire.units;

import com.noimg.starfire.Card;
import com.noimg.starfire.Team;
import com.noimg.starfire.world.Star;

import java.util.ArrayList;
import java.util.List;

public class Squad {
	public final Card card;
	public final Team team;
	public final List<Unit> units = new ArrayList<Unit>();

	public Squad(Card card, Team team) {
		this.card = card;
		this.team = team;
	}

	public void move(Star star) {
		for (Unit unit : units) {
			unit.move(star.position);
		}
	}

	public static void newSquad (Card card, Team team) {

	}
}
