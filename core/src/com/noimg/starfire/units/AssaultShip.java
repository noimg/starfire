package com.noimg.starfire.units;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;

public class AssaultShip extends Unit {

	public AssaultShip(float x, float y, Squad team) {
		super(x, y, team);
	}

	/**
	 * fixme todo любой юнит (включая вражеский) зависит от карты, которая имеется у игрока
	 * на данном этапе это фиксится team != playerTeam, но вскоре нужно будет  придумать как реализовать
	 * дефолтные (и улучшаемые) значения для врагов
	 */

	@Override
	public int getDefaultHealth() {
		return Card.ASSAULT_SHIP.getIntProperty("health");
	}

	@Override
	public int getDefaultDamage() {
		return Card.ASSAULT_SHIP.getIntProperty("damage");
	}

	@Override
	public float getDefaultSpeed() {
		return Card.ASSAULT_SHIP.getIntProperty("speed");
	}

	@Override
	public int getDefaultRange() {
		return 120;
	}

	@Override
	public float getDefaultReloadTime() {
		return 0.3f;
	}

	@Override
	public Texture getTexture() {
		return Assets.Textures.ASSAULT_SHIP;
	}

	@Override
	public Sound getShootSound() {
		return Assets.Sounds.SHOOT_1;
	}

	@Override
	public DeathEvent getDeathEvent() {
		return null;
	}
}
