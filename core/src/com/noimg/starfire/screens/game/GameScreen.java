package com.noimg.starfire.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;
import com.noimg.starfire.CardDeck;
import com.noimg.starfire.SmoothCameraInput;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.utils.Debug;
import com.noimg.starfire.utils.Utils;
import com.noimg.starfire.world.Star;
import com.noimg.starfire.world.World;
import com.noimg.starfire.world.WorldSettings;
import com.noimg.starfire.collisions.CollisionGrid;
import com.noimg.starfire.collisions.CollisionGridCell;

/**
 * Экран, в котором обрабатывается основная логика игры
 */

public class GameScreen implements Screen {

	public final SmoothCameraInput camera;
	public final com.noimg.starfire.Selector selector;
	public final GameUI ui;
	public final com.noimg.starfire.world.World world;
	private final ShapeRenderer shaper;
	private final Viewport viewport;
	private final Debug debug;
	private final GameInput input;
	public Team playerTeam;
	public Sound threadSound;

	public GameScreen(WorldSettings settings) {
		this.playerTeam = settings.playerTeam;
		world = new World(settings);
		Gdx.gl.glLineWidth(3);
		shaper = new ShapeRenderer();
		debug = new Debug(Assets.Fonts.MOVAVI);
		OrthographicCamera cam = new OrthographicCamera();
		camera = new SmoothCameraInput(cam);
		viewport = new ScreenViewport(cam);
		selector = new com.noimg.starfire.Selector(shaper);
		ui = new GameUI();

		/* Инициализация ввода */
		input = new GameInput();
		InputMultiplexer inputMultiplexer = new InputMultiplexer();
		inputMultiplexer.addProcessor(ui.stage);
		inputMultiplexer.addProcessor(camera);
		inputMultiplexer.addProcessor(input);
		Gdx.input.setInputProcessor(inputMultiplexer);
	}

	public final void initTeams() {
		for (Team team : Team.values()) {
			for (int i = 0; i < Starfire.CARDS_IN_DECK; i++) {
				team.cards[i] = Card.values()[Starfire.random.nextInt(Card.values().length)];
				//team.cards[i] = Starfire.random.nextBoolean() ? Card.ASSAULT_SHIP : Card.KAMIKADZE_SHIP;
			}

			team.deck = new CardDeck(team);
		}
	}

	@Override
	public void show() {
		viewport.apply();
	}

	@Override
	public void render(float delta) {
		logic(delta);
		draw(delta);
		playThreadSound();
		debug.update(delta);
		ui.update(delta);
	}

	private void playThreadSound () {
		if (threadSound != null) {
			threadSound.play();
			threadSound = null;
		}
	}

	private void logic(float delta) {
		camera.update(delta);
		input.update(delta);
		world.logic(delta);
	}

	private void draw(float delta) {
		Gdx.gl.glClearColor(Assets.Colors.BLACK.r, Assets.Colors.BLACK.g, Assets.Colors.BLACK.b, 1);
		//Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT | (Gdx.graphics.getBufferFormat().coverageSampling?GL20.GL_COVERAGE_BUFFER_BIT_NV:0));
		world.draw(delta);
		shaper.setProjectionMatrix(camera.cam.combined);
		shaper.begin(ShapeRenderer.ShapeType.Line);
		if (Starfire.SHOW_GRID) drawGrid();
		if (Starfire.SHOW_RANGES) drawRanges();
		if (Starfire.SHOW_FORCES) drawForces();
		selector.draw(delta);
		shaper.setColor(Color.WHITE);
		shaper.rect(0, 0, world.width, world.height);
		shaper.end();
	}

	private void drawForces() {
		for (Unit unit : world.units) {
			shaper.line(unit.position.x, unit.position.y, unit.position.x + unit.getForce().x * 10, unit.position.y + unit.getForce().y * 10);
		}
	}

	private void drawRanges() {
		for (Unit unit : world.units) {
			shaper.circle(unit.position.x, unit.position.y, unit.getDefaultRange());
		}
	}

	private void drawGrid() {
		for (CollisionGrid.CollisionGridThread thread : world.collisionGrid.threads) {
			shaper.setColor(thread.color);
			for (CollisionGridCell cell : thread.cells) {
				shaper.rect(cell.x * Starfire.COLLISION_CELL_SIZE, cell.y * Starfire.COLLISION_CELL_SIZE,
						Starfire.COLLISION_CELL_SIZE, Starfire.COLLISION_CELL_SIZE);
			}
		}
	}

	/**
	 * @return true если карта была использована успешно
	 */
	public boolean useUnitCard(Card card, Star star, Team team) {
		if (card.type != Card.Type.UNIT) {
			return false;
		}

		Squad squad = new Squad(card, team);

		// Если star - союзная звезда, то развертываем отряд на ней, если же нет - то развертываем
		// отряд на ближайшей союзной звезде
		Star deployStar = star.getTeam() == team ? star : world.getClosestStar(star);
		if (deployStar == null) return false;

		int deployAmount = card.getIntProperty("deployamount");
		for (int i = 0; i < deployAmount; i++) {
				Vector2 point = deployAmount == 1 ?
						Utils.randomPointInCircle(deployStar.position.x, deployStar.position.y, 400) :
						Utils.pointInCircle(deployStar.position.x, deployStar.position.y, deployAmount / 2, i, 400);

				Unit unit = Unit.newUnit(card, point.x, point.y, squad);
				if (unit == null) return false;
				unit.move(star.position);
		}

		deployStar.addSquadToDeployQueue(squad);
		return true;
	}

	/**
	 * @return true если карта была использована успешно
	 */
	public boolean useTrickCard(float x, float y, Card card, Star star, Team team) {
		if (card.type != Card.Type.TRICK) {
			return false;
		}

		card.action.action(x, y, world, star, team);
		return true;
	}

	/**
	 * @return true если карта была использована успешно
	 */
	public boolean useCard(Card card, float x, float y, Team team) {
		if (team.deck.selected == -1) return false;
		Vector2 vec = new Vector2(x, y);

		Star closestStar = null;
		for (Star star : world.stars) {
			if (star.inRange(vec)) {
				closestStar = star;
			}
		}

		boolean success = false;
		switch (card.type) {
			case UNIT:
				success = useUnitCard(card, closestStar, playerTeam);
				break;

			case TRICK:
				success = useTrickCard(x, y, card, closestStar, playerTeam);
				break;
		}

		if (success) {
			team.subResources(card.getIntProperty("cost"));
			if (team == playerTeam) {
				team.deck.removeSelectedCard();
				team.deck.nextCard();
			}

			team.deck.selected = -1;
		}

		return success;
	}

	@Override
	public void resize(int width, int height) {
		debug.resize(width, height);
		ui.resize(width, height);
		viewport.update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {
		shaper.dispose();
		debug.stage.dispose();
	}
}