package com.noimg.starfire.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Card;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.utils.RadialSprite;
import com.noimg.starfire.utils.Utils;

public class GameUI {
	public final Stage stage;
	public final ImageButton unselectButton;
	private final Viewport viewport;
	private final Label.LabelStyle labelStyle;
	private final VerticalGroup notificationTable;
	private final CardStack[] cardStacks = new CardStack[4];
	private final VerticalGroup sideGroup;
	private float cardReloadTimer;
	private final VerticalGroup resourceTable;
	private final Table resourceLabelTable;
	private final Label resourceLabel;

	public void updateResourceTabs () {
		resourceTable.clear();
		resourceLabel.setText(Starfire.game.playerTeam.getResources() + "");
		for (int i = 0; i < Starfire.game.playerTeam.getResources(); i++) {
			Image resourceTab = new Image(Assets.Textures.RESOURCE_BAR);
			resourceTable.right().bottom().padLeft(5).addActor(resourceTab);
			/*
			resourceTab.setOrigin(resourceTab.getWidth() / 2, resourceTab.getHeight() / 2);
			resourceTab.addAction(Actions.sequence(
					Actions.color(new Color(1, 1, 1, 0)),
					Actions.parallel(
							Actions.scaleBy(1.25f, 1.25f, 0.25f),
							Actions.fadeIn(0.25f)
					),
					Actions.scaleTo(1, 1, 1.25f)
				)
			);*/
		}
	}

	GameUI() {
		viewport = new FitViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage = new Stage(viewport);
		labelStyle = new Label.LabelStyle();
		labelStyle.font = Assets.Fonts.MOVAVI;

		resourceTable = new VerticalGroup();
		resourceTable.setFillParent(true);
		resourceTable.space(5);
		resourceTable.setTouchable(Touchable.disabled);

		resourceLabel = new Label("0", labelStyle);
		resourceLabelTable = new Table();
		resourceLabelTable.setFillParent(true);
		resourceLabelTable.right().top().pad(10).add(resourceLabel);

		sideGroup = new VerticalGroup();
		sideGroup.setFillParent(true);

		notificationTable = new VerticalGroup();
		notificationTable.setFillParent(true);
		notificationTable.setTouchable(Touchable.disabled);
		notificationTable.center().bottom().pad(30);

		unselectButton = new ImageButton(new TextureRegionDrawable(new TextureRegion(Assets.Textures.UNSELECT_BUTTON)));
		unselectButton.setVisible(false);
		unselectButton.addListener(new ClickListener(Input.Buttons.LEFT) {
			@Override
			public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
				Starfire.game.selector.unselectAll();
				return true;
			}
		});

		Table unselectButtonTable = new Table();
		unselectButtonTable.setFillParent(true);
		unselectButtonTable.setTouchable(Touchable.childrenOnly);
		unselectButtonTable.add(unselectButton);
		unselectButtonTable.center().right();
		unselectButtonTable.pad(20);

		stage.addActor(unselectButtonTable);
		stage.addActor(sideGroup);
		stage.addActor(notificationTable);
		stage.addActor(resourceTable);
		stage.addActor(resourceLabelTable);
		//stage.setDebugAll(true);
	}

	public void addCard(final Card card, final int id) {
		if (cardStacks[id] != null)
			sideGroup.removeActor(cardStacks[id].stack, false);

		/*
		for (int i = 0; i < cardStacks.length; i++) {
			if (i > id && cardStacks[i] != null) {
				cardStacks[i].stack.addAction(Actions.sequence(
						Actions.moveBy(0, 140, 0.5f), new Action() {
							@Override
							public boolean act(float delta) {
								cardStacks[id] = new CardStack(card, id);
								sideGroup.addActor(cardStacks[id].stack);
								return true;
							}
						}));
			}
		}*/

		cardStacks[id] = new CardStack(card, id);
		sideGroup.left().addActor(cardStacks[id].stack);
	}

	// <editor-fold desc="UI">
	public void addNotification(String text, float time) {
		Label label = new Label(text, labelStyle);
		label.addAction(Actions.sequence(
				Actions.color(new Color(1, 1, 1, 0)),
				Actions.fadeIn(0.15f, Interpolation.pow3),
				Actions.delay(time),
				Actions.parallel(
						Actions.fadeOut(0.5f, Interpolation.pow3),
						Actions.moveBy(0, 18, 0.5f, Interpolation.pow3)
				),
				Actions.removeActor()));
		label.setAlignment(Align.center);
		notificationTable.addActor(label);
	}

	private Image addResourceIcon(Table table) {
		Image resIcon = new Image(Assets.Textures.RESOURCE);
		table.add(resIcon).size(16, 16).padLeft(5);
		resIcon.setName("ResourceIcon");
		return resIcon;
	}

	/*
	public void addPopup (float x, float y, String text) {
		Label label = new Label(text, labelStyle);
		Vector3 screen = game.camera.cam.project(new Vector3(x, y, 0));
		Vector2 stageCoordinates = stage.screenToStageCoordinates(new Vector2(screen.x, screen.y));
		label.setOrigin(stageCoordinates.x, stageCoordinates.y);
		stage.addActor(label);
	}*/
	// </editor-fold>

	private void smoothResourceCounter(float delta) {
		/*
		if (Math.abs (Starfire.game.playerTeam.resources - currentResource) > 2) {
			timer += delta;
			currentResource = (int) Utils.lerp(currentResource, Starfire.game.playerTeam.resources, timer / 4f);
		} else {
			currentResource = Starfire.game.playerTeam.resources;
			timer = 0;
		}*/

		//resourceLabel.setText(Starfire.game.playerTeam.getResources() + "");
	}

	private void reloadCards(float delta) {
		if (cardReloadTimer > 0) {
			cardReloadTimer -= delta;

			if (cardReloadTimer <= 0) {
				cardReloadTimer = 0;
			}
		}
	}

	public void update(float delta) {
		for (int i = 0; i < cardStacks.length; i++) {
			if (i == Starfire.game.playerTeam.deck.selected) continue;
			CardStack cardStack = cardStacks[i];
			if (cardStack == null) continue;
			boolean enoughResources = Starfire.game.playerTeam.getResources() >= cardStack.card.getIntProperty("cost");
			cardStack.stack.addAction(Actions.color(enoughResources && cardReloadTimer <= 0 ? Assets.Colors.WHITE : new Color(1, 1, 1, 0.5f), 0.125f));
			cardStack.cost.addAction(Actions.color(enoughResources ? Assets.Colors.WHITE : Assets.Colors.RED, 0.125f));
			cardStack.resourceIcon.addAction(Actions.color(enoughResources ? Assets.Colors.WHITE : Assets.Colors.RED, 0.125f));
		}

		reloadCards(delta);
		smoothResourceCounter(delta);
		stage.act(delta);
		stage.draw();
	}

	public final boolean isMouseInUI() {
		return Gdx.input.getX() < 200;
	}

	public void resize(int width, int height) {
		viewport.update(width, height, true);
	}

	private class CardStack {
		public final Card card;
		public final Stack stack;
		public final Image image;
		public final Label name;
		public final Label cost;
		public final Table imageTable;
		public final Table nameTable;
		public final Table costTable;
		public final Image resourceIcon;
		public final RadialSprite radialSprite;

		CardStack(final Card card, final int id) {
			this.card = card;

			radialSprite = new RadialSprite(Assets.Textures.CARD_MASK);
			radialSprite.getTexture();
			final Image image = new Image(card.texture);
			image.setName("CardImage");
			image.setScaling(Scaling.fit);
			this.image = image;

			final Label name = new Label(card.name, labelStyle);
			name.setName("CardName");
			name.setAlignment(Align.center);
			name.setWrap(true);
			this.name = name;

			final Label cost = new Label(card.getIntProperty("cost") + "", labelStyle);
			cost.setName("CardCost");
			cost.setAlignment(Align.center);
			this.cost = cost;

			imageTable = new Table();
			imageTable.add(image).size(65, 65);

			nameTable = new Table();
			nameTable.top().pad(10).add(name);

			costTable = new Table();
			costTable.bottom().pad(10).add(cost);
			resourceIcon = addResourceIcon(costTable);

			Image cardImage = new Image(Assets.Textures.CARD);
			cardImage.setScaling(Scaling.fit);
			stack = new Stack();
			stack.add(cardImage);
			stack.add(imageTable);
			stack.add(nameTable);
			stack.add(costTable);

			for (Actor actor : stack.getChildren()) {
				actor.addAction(Actions.sequence(Actions.color(Assets.Colors.TRANSPARENT_WHITE), Actions.moveBy(-300, 0), Actions.parallel(Actions.fadeIn(0.25f), Actions.moveBy(300, 0, 0.25f))));
			}

			stack.addListener(new DragListener() {
				private boolean canDrag = true;

				public void drag(InputEvent event, float x, float y, int pointer) {
					if (!canDrag || Starfire.game.playerTeam.getResources() < card.getIntProperty("cost") || cardReloadTimer > 0)
						return;
					stack.moveBy(x - stack.getWidth() / 2, y - stack.getHeight() / 2);
					//todo show unit/building future location
				}

				public void dragStart(InputEvent event, float x, float y, int pointer) {
					if (!canDrag || Starfire.game.playerTeam.getResources() < card.getIntProperty("cost") || cardReloadTimer > 0)
						return;
					Starfire.game.playerTeam.deck.selected = id;
					//stack.setOrigin(stack.getHeight() * 1.25f / 2, stack.getWidth() * 1.25f / 2);
					stack.addAction(Actions.color(new Color(1, 1, 1, 0.5f), 0.125f));
					stack.toFront();

					stack.addAction(Actions.moveBy(300, 0, 0.25f));
					sideGroup.addAction(Actions.moveBy(-300, 0, 0.25f));

					for (Actor actor : stack.getChildren()) {
						//actor.addAction(Actions.scaleTo(1.25f, 1.25f, 0.125f));
					}
				}

				public void dragStop(InputEvent event, float x, float y, int pointer) {
					if (!canDrag || Starfire.game.playerTeam.getResources() < card.getIntProperty("cost") || cardReloadTimer > 0)
						return;
					canDrag = false;
					stack.addAction(Actions.moveBy(-300, 0, 0.25f));
					sideGroup.addAction(Actions.moveBy(300, 0, 0.25f));

					Vector2 position = Utils.screenToWorld(Gdx.input.getX(), Gdx.input.getY());
					boolean success = Starfire.game.useCard(card, position.x, position.y, Starfire.game.playerTeam);

					if (stack.getX() < 120 || !success) {
						addCard(Starfire.game.playerTeam.deck.cards[id], id);
						return;
					}

					for (Actor actor : stack.getChildren()) {
						actor.setOrigin(actor.getWidth() / 2, actor.getHeight() / 2);
						actor.addAction(
								Actions.sequence(
										Actions.parallel(Actions.scaleTo(0, 0, 0.125f), Actions.fadeOut(0.125f)),
										new Action() {
											@Override
											public boolean act(float delta) {
												addCard(Starfire.game.playerTeam.deck.cards[id], id);
												canDrag = true;
												cardReloadTimer = Starfire.CARD_RELOAD_TIME;
												return true;
											}
										}
								)
						);
					}
				}
			});
		}
	}
}
