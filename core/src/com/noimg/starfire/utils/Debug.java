package com.noimg.starfire.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.noimg.starfire.Starfire;

/**
 * Класс для отрисовки на экран некоторых вещей необхоимых для отладки
 */

public class Debug {
	public final Stage stage;
	private final Label framesPerSecond;
	private final Label units;
	private final Label unit;
	private final int processors;

	public Debug(BitmapFont font) {
		processors = Runtime.getRuntime().availableProcessors();
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);

		// Создаем textStyle для дебага используя данный в конструкторе шрифт.
		// В будущем, скорее всего, textStyle будет заменен полноценным skin'ом
		Label.LabelStyle textStyle;
		textStyle = new Label.LabelStyle();
		textStyle.font = font;
		textStyle.font.getData().markupEnabled = true;

		framesPerSecond = new Label("FPS", textStyle);
		unit = new Label("Unit Info", textStyle);
		units = new Label("Units", textStyle);

		Table table = new Table();
		table.setFillParent(true);
		table.bottom().right();
		table.add(framesPerSecond).minHeight(20).align(Align.right).row();
		table.add(unit).minHeight(20).align(Align.right).row();
		table.add(units).minHeight(20).align(Align.right).row();
		table.pad(20);
		stage.addActor(table);
	}

	public void update(float delta) {
		framesPerSecond.setText("FPS [#BE2633]" + Gdx.graphics.getFramesPerSecond());
		if (Starfire.game.world.squads.size() > 0)
			unit.setText("Squad Info " + Starfire.game.world.squads.get(0).units.size());
		else
			unit.setText("squads is zeor");
		units.setText("Units [#BE2633]" + Starfire.game.world.units.size());
		stage.act(delta);
		stage.draw();
	}

	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}
}
