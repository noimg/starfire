package com.noimg.starfire.world;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.noimg.starfire.collisions.GameObject;

/**
 * Физическое тело. Предоставляет методы и поля для того что бы физика работала на объект
 */

public class PhysicsBody extends com.noimg.starfire.collisions.GameObject {
	public final int radiusSquared;
	private final Vector2 force;
	// Число от 0 до 1, где значения, близкие к 1 дают меньшее сопротивление инерции (1 = полная инерция)
	public float inertia;
	public boolean solid;

	public PhysicsBody(float x, float y, int radius, float inertia) {
		super(x, y, radius);
		force = new Vector2(0, 0);
		this.inertia = inertia;
		radiusSquared = radius * radius;
		solid = true;
	}

	public PhysicsBody(float x, float y, int radius) {
		this(x, y, 0, radius);
	}

	public PhysicsBody(float x, float y) {
		this(x, y, 0, 1f);
	}

	@Override
	public void updateCollision(GameObject nearestGameObject) {

	}

	@Override
	public void destroy() {

	}

	/**
	 * Применяет силу в определенном направлении
	 *
	 * @param direction Направление
	 * @param amount    Множитель силы, или же скорость
	 */
	public void addDirectionForce(Vector2 direction, float amount) {
		// Используем nor() для того, что бы значительные численные различия в направлении не давали различий в силе
		//force.add (direction.nor().scl (amount));
		force.add(direction.nor().scl(amount));
	}

	public void addDirectionForce(Vector3 direction, float amount) {
		addDirectionForce(new Vector2(direction.x, direction.y), amount);
	}

	/**
	 * Применяет силу в направлении точки
	 *
	 * @param point  Точка в пространстве
	 * @param amount Множитель силы, или же скорость
	 */
	public void addPointForce(Vector2 point, float amount) {
		Vector2 direction = new Vector2(point.x - position.x, point.y - position.y);
		addDirectionForce(direction, amount);
	}

	public Vector2 getForce() {
		return force;
	}

	public void updatePhysics() {
		position.add(force);
		force.scl(inertia);
		/*
		idle = force.len2() < 900f;

		if (!idle) {
			position.add(force);
			force.scl(inertia);
		} else {
			force.setZero();
		}*/
	}
}
