package com.noimg.starfire.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.world.Explosion;
import com.noimg.starfire.world.Star;

/**
 * Обрабатывает ввод
 * <p>
 * Одинарный тап на пустом месте или на звезде 	- послать туда юнитов
 * Одинарный тап на юните 					- выделить юнит отдельно
 * Длительный тап на пустом месте 				- начать выделение юнитов
 * Длительный тап на звезде 					- показать инфу о звезде
 * Длительный тап на юните 					- показать инфу о юните
 * Двоной тап на юните 						- выделить всех подобных юнитов
 * Двойной тап на пустом месте 				- выделить всех юнитов
 * Смахнуть 									- контроль камеры
 * <p>
 * todo двигать камеру, когда:
 * палец у границ экрана при выборе карты / выборе юнитов
 */
class GameInput extends InputAdapter {

	private boolean activeTouch;
	private float touchHoldTimer;    // Таймер для реализации удерживания
	private float selectionTimer;    // Таймер для оптимизированной рисовки многоугольного выделения
	private float startHoldX;
	private float startHoldY;
	private boolean onUnit;
	private boolean doubleTapped;
	private boolean longTapped;
	private int touchCount;


	@Override
	public boolean keyDown(int keycode) {
		switch (keycode) {
			case Input.Keys.G:
				Starfire.SHOW_GRID = !Starfire.SHOW_GRID;
				break;
			case Input.Keys.R:
				Starfire.SHOW_RANGES = !Starfire.SHOW_RANGES;
				break;
			case Input.Keys.F:
				Starfire.SHOW_FORCES = !Starfire.SHOW_FORCES;
				break;
			case Input.Keys.M:
				Starfire.game.playerTeam.addResources(10);
				break;
			case Input.Keys.Y:
				Vector3 a = Starfire.game.camera.cam.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
				Vector2 b = new Vector2(a.x, a.y);
				for (Unit unit : Starfire.game.world.units) {
					unit.move(b);
				}
				break;
			case Input.Keys.NUM_2:
				Starfire.game.playerTeam = Team.GREEN;
				break;
			case Input.Keys.NUM_1:
				Starfire.game.playerTeam = Team.RED;
				break;
		}

		return false;
	}

	public void update(float delta) {
		Vector3 position = Starfire.game.camera.cam.unproject(new Vector3(Gdx.input.getX(), Gdx.input.getY(), 0));
		float x = position.x;
		float y = position.y;

		// Одинарный тап
		if (Gdx.input.justTouched()) {
			touchCount++;
			startHoldX = Gdx.input.getX();
			startHoldY = Gdx.input.getY();

			// Одинарный тап на юните
			onUnit = false;
			Unit unit = Starfire.game.world.getUnitOnPosition(x, y);
			if (unit != null) {
				if (unit.team == Starfire.game.playerTeam) {
					onUnit = true;
					if (Starfire.game.selector.selectedSquads.contains(unit.squad)) {
						Starfire.game.selector.unselectSquad(unit.squad);
					} else {
						Starfire.game.selector.unselectAll();
						Starfire.game.selector.selectSquad(unit.squad);
					}
				}
			}

			//System.out.println(Starfire.game.world.collisionGrid.getNearestGameObjects(x, y).size());
		}

		if (touchCount > 0) {
			touchHoldTimer += delta;

			if (touchHoldTimer < 0.23f) {
				if (touchCount >= 2) {
					doubleTapped = true;
					if (Starfire.game.selector.selectedSquads.isEmpty()) {
						// Если двойной тап был на пустом месте, то выделяем всех юнитов
						Starfire.game.selector.selectAll();
					}

					Explosion.newThreadSafeExplosion(x, y, Explosion.Power.NORMAL, true);
					touchHoldTimer = 0;
					touchCount = 0;
				} else {
					doubleTapped = false;
				}
			} else {
				touchCount = 0;
				touchHoldTimer = 0;
			}
		}

		boolean moved = Math.abs(startHoldX - Gdx.input.getX()) > 20 || Math.abs(startHoldY - Gdx.input.getY()) > 20;
		if (Gdx.input.isTouched(0)) {
			touchHoldTimer += delta;
			activeTouch = true;

			if (Starfire.game.selector.isSelecting) {
				Starfire.game.camera.canMove = false;
				if (Starfire.SELECTION_TYPE == Starfire.SelectionType.POLYGON) {
					selectionTimer += delta;
					if (selectionTimer > 0.04f) {
						selectionTimer = 0;
						Starfire.game.selector.selectionPoints.add(new Vector2(x, y));
					}
				} else if (Starfire.SELECTION_TYPE == Starfire.SelectionType.RECTANGLE) {
					if (Starfire.game.selector.selectionPoints.size() == 1) {
						Starfire.game.selector.selectionPoints.add(new Vector2());
					}
					Starfire.game.selector.selectionPoints.set(1, new Vector2(x, y));
				}
			} else {
				if (moved) {
					touchHoldTimer = 0;
				}

				if (touchHoldTimer > 0.3f && !longTapped) {
					touchHoldTimer = 0;
					longTapped = true;

					Vector2 pos = new Vector2(x, y);
					Star tappedStar = null;
					for (Star star : Starfire.game.world.stars) {
						if (star.inRange(pos)) {
							tappedStar = star;
							break;
						}
					}

					if (tappedStar == null) {
						Starfire.game.selector.isSelecting = true;
						Starfire.game.ui.addNotification("You can select now", 1);

						if (Starfire.SELECTION_TYPE == Starfire.SelectionType.RECTANGLE) {
							Starfire.game.selector.selectionPoints.add(new Vector2(x, Gdx.graphics.getHeight() - y));
						}
						Starfire.game.selector.unselectAll();
					} else {
						//camera.body.position.set (tappedStar.position);
						//camera.startSmoothZoom(Starfire.MIN_ZOOM, camera.cam.zoom, 0.7f);
						//camera.smoothMove(tappedStar.position);
						Starfire.game.ui.addNotification("Star tap", 1);
					}
				}
			}
		} else if (activeTouch) {
			activeTouch = false;
			touchHoldTimer = 0;
			longTapped = false;

			if (Starfire.game.selector.isSelecting) {
				Starfire.game.selector.select();
				Starfire.game.selector.isSelecting = false;
				Starfire.game.camera.canMove = true;
			} else {
				if (!onUnit && !moved && !doubleTapped) {
					Vector2 vec = new Vector2(x, y);

					for (Star star : Starfire.game.world.stars) {
						if (star.inRange(vec)) {
							for (Squad squad : Starfire.game.selector.selectedSquads) {
								Starfire.game.world.moveSquad(x, y, squad);
							}
						}
					}
				}
			}
		}
	}
}
