package com.noimg.starfire.world;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Popup;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.collisions.GameObject;
import com.noimg.starfire.units.Projectile;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.world.maps.Map;

import java.util.ArrayList;
import java.util.List;

public class World {
	public final com.noimg.starfire.collisions.CollisionGrid collisionGrid;
	public final int width, height;
	public final List<Unit> units = new ArrayList<Unit>();
	public final List<Squad> squads = new ArrayList<Squad>();
	public final List<Blackhole> blackholes = new ArrayList<Blackhole>();
	public final List<Wormhole> wormholes = new ArrayList<Wormhole>();
	public final List<Star> stars = new ArrayList<Star>();
	public final List<Wall> walls = new ArrayList<Wall>();
	public final List<Explosion> explosions = new ArrayList<Explosion>();
	public final List<Projectile> projectiles = new ArrayList<Projectile>();
	public final List<Popup> popups = new ArrayList<Popup>();    //todo move to game.ui
	private final SpriteBatch batch;
	public final ShapeRenderer shaper;
	public final Map map;

	public World(WorldSettings settings) {
		map = settings.map;
		width = map.getWidth();
		height = map.getHeight();
		collisionGrid = new com.noimg.starfire.collisions.CollisionGrid(width / Starfire.COLLISION_CELL_SIZE, height / Starfire.COLLISION_CELL_SIZE);

		batch = new SpriteBatch();
		shaper = new ShapeRenderer();
	}

	public void generate() {
		collisionGrid.initThreading();
		map.generate();

		for (Star star : stars) {
			if (star.getTeam() == Starfire.game.playerTeam) {
				Starfire.game.camera.body.position.set(star.position);
			}
		}
	}

	public Unit getUnitOnPosition(float x, float y) {
		com.noimg.starfire.collisions.GameObject[] nearestGameObjects = collisionGrid.getGameObjectsInRange(x, y, 100);
		if (nearestGameObjects == null) return null;

		for (GameObject go : nearestGameObjects) {
			if (!(go instanceof Unit)) continue;
			Unit unit = (Unit) go;
			// Расширяем зону тапа на юнита в 1.5 раза, т.к. иначе по нему трудно попасть, особенно
			// с тачсрина
			Rectangle rect = unit.sprite.getBoundingRectangle();
			rect.setSize(rect.width * 1.5f, rect.height * 1.5f);
			rect.setCenter(unit.position);
			if (rect.contains(new Vector2(x, y))) {
				return unit;
			}
		}

		return null;

		//GameObject gameObjectOnPosition = getGameObjectOnPosition(x, y);
		//if (gameObjectOnPosition instanceof Unit) return (Unit)gameObjectOnPosition;
		//else return null;
	}
/*
	public GameObject getGameObjectOnPosition(float x, float y) {
		List<GameObject> nearestGameObjects = collisionGrid.getNearestGameObjects(x, y);
		if (nearestGameObjects == null) return null;

		for (GameObject go : nearestGameObjects) {
			Rectangle rect = new Rectangle(go.position.x - go.radius, go.position.y - go.radius, go.radius * 2, go.radius * 2);
			if (rect.contains(new Vector2(x, y))) {
				return go;
			}
		}

		return null;
	}*/

	public void logic(float delta) {
		collisionGrid.update(delta);

		// Поп-апы
		for (int i = 0, size = popups.size(); i < size; i++) {
			Popup popup = popups.get(i);
			if (popup.disappear) {
				popups.remove(popup);
				size--;
			}
		}

		// Снаряды
		for (int i = 0, size = projectiles.size(); i < size; i++) {
			Projectile projectile = projectiles.get(i);
			if (projectile != null && projectile.disappear) {
				projectiles.remove(projectile);
				size--;
			}
		}
	}

	public void draw(float delta) {
		shaper.setProjectionMatrix(Starfire.game.camera.cam.combined);
		shaper.begin(ShapeRenderer.ShapeType.Line);
		batch.setProjectionMatrix(Starfire.game.camera.cam.combined);
		batch.begin();

		// BOTTOM LAYER
		for (Blackhole blackhole : blackholes) {
			blackhole.draw(batch);
		}

		for (Unit unit : units) {
			unit.sprite.draw(batch);

			if (unit.dead) {
				//unit.sprite.setAlpha(unit.sprite.getColor().a - 0.05f);
			}
		}

		for (Wormhole wormhole : wormholes) {
			wormhole.draw(batch);
		}

		for (Star star : stars) {
			for (Star neighbor : star.neighbors) {
				shaper.line(star.position.x + (neighbor.position.x - star.position.x) / 10f, star.position.y + (neighbor.position.y - star.position.y) / 10f, neighbor.position.x, neighbor.position.y, star.getTeam().color, neighbor.getTeam().color);
			}
			star.draw(batch, Starfire.game.camera.cam.zoom);
		}

		for (Explosion explosion : explosions) {
			explosion.draw(batch, delta);
		}

		for (int i = 0; i < projectiles.size(); i++) {
			Projectile projectile = projectiles.get(i);
			if (projectile != null) projectile.draw(delta, shaper);
		}

		for (Wall wall : walls) {
			wall.draw(shaper, delta);
		}

		for (Popup popup : popups) {
			popup.draw(batch, delta, Starfire.game.camera.cam.zoom);
		}
		// TOP LAYER

		batch.end();
		shaper.end();
	}

	public void moveSquad(float x, float y, Squad squad) {
		/**
		 * (!!!)
		 * Юниты должны следовать маршруту neighbor'ов, т.е. двигаться по линиям: если же юнитов
		 * послать из одного место в другое, где другое место хоть и соединено с союзными звездами,
		 * оно все равно находится не на ближайшей звезде той звезды, с которой летит юнит
		 */
		Vector2 pos = new Vector2(x, y);
		for (Star star : stars) {
			if (star.inRange(pos)) {
				//if (star.getTeam() == playerTeam || star.hasSideNeighbors(playerTeam)) {
				if (true) {
					squad.move(star);
				}
			}
		}
	}

	/**
	 * @return Ближайшая звезда независимо от стороны, которой она подконтрольна
	 */
	public Star getClosestStar(Star star) {
		Star closest = null;
		float minDst = Float.MAX_VALUE;
		for (Star s : stars) {
			float dst = s.position.dst(star.position);
			if (s != star && dst < minDst) {
				minDst = dst;
				closest = s;
			}
		}

		return closest;
	}

	public Star getClosestStar(Star star, Team team) {
		Star closest = null;
		float minDst = Float.MAX_VALUE;
		for (Star neighbor : star.neighbors) {
			if (neighbor.getTeam() != team) continue;
			float dst = neighbor.position.dst(star.position);
			if (neighbor != star && dst < minDst) {
				minDst = dst;
				closest = neighbor;
			}
		}

		return closest;
	}

	/**
	 * updateThreaded вызывается из CollisionGrid в промежутках между коллизиями для
	 * потокобезопасности. В основном тут обрабатываются апдейты GameObject'ов.
	 */
	public void updateThreaded(float delta, com.noimg.starfire.collisions.CollisionGrid.CollisionGridThread[] threads) {
		// Кротовые норы
		for (int i = 0, wormholesSize = wormholes.size(); i < wormholesSize; i++) {
			Wormhole wormhole = wormholes.get(i);
			wormhole.update(delta);
		}

		// Черные дыры
		for (int i = 0, size = blackholes.size(); i < size; i++) {
			Blackhole blackhole = blackholes.get(i);

			if (blackhole.dead) {
				blackhole.destroy();
				size--;
			}

			blackhole.update(delta);
		}

		// Взрывы
		for (int i = 0, size = explosions.size(); i < size; i++) {
			Explosion explosion = explosions.get(i);

			if (explosion.dead) {
				explosion.destroy();
				size--;
			}
		}

		// Юниты
		for (int i = 0, size = units.size(); i < size; i++) {
			Unit unit = units.get(i);

			if (unit.dead) {
				unit.destroy();
				size--;
				continue;
			}

			collisionGrid.remove(unit);
			unit.update(delta);
			collisionGrid.insert(unit);
		}

		// Звезды
		for (Star star : stars) {
			star.update(delta);
		}
	}
}