package com.noimg.starfire;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.units.Squad;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.utils.QuickHull;
import com.noimg.starfire.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class Selector {

	public final List<Squad> selectedSquads = new ArrayList<Squad>();
	public final ArrayList<Vector2> selectionPoints = new ArrayList<Vector2>();
	private final ShapeRenderer shaper;
	private final QuickHull quickHull;
	public boolean isSelecting;

	public Selector(ShapeRenderer shaper) {
		this.shaper = shaper;
		quickHull = new QuickHull();
	}

	public void draw(float delta) {
		shaper.setColor(Assets.Colors.WHITE);

		if (isSelecting) {
			drawSelection();
		}

		if (!selectedSquads.isEmpty()) {
			drawSelectedSquads();
		}
	}

	private void drawSelectedSquads() {
		ArrayList<Vector2> points = new ArrayList<Vector2>();
		for (Squad squad : selectedSquads) {
			for (Unit unit : squad.units) {
				points.add(unit.position);

				final int padding = (int) (unit.getDefaultRange() / 1.5f);
				for (int angle = 0; angle < 8; angle++) {
					float i = unit.position.x + padding * (float) Math.cos(angle * 45);
					float j = unit.position.y + padding * (float) Math.sin(angle * 45);
					points.add(new Vector2(i, j));
				}
			}
		}

		ArrayList<Vector2> quickHullPoints = quickHull.get(points);
		if (quickHullPoints.isEmpty()) return;

		for (int i = 0, pSize = quickHullPoints.size(); i < pSize; i++) {
			if (i + 1 < pSize) {
				shaper.line(quickHullPoints.get(i), quickHullPoints.get(i + 1));
			}
		}

		shaper.line(quickHullPoints.get(0), quickHullPoints.get(quickHullPoints.size() - 1));
	}

	private void drawSelection() {
		if (Starfire.SELECTION_TYPE == Starfire.SelectionType.POLYGON) {
			int selectionPointsSize = selectionPoints.size();
			for (int i = 0; i < selectionPointsSize; i++) {
				if (i + 1 < selectionPointsSize)
					shaper.line(selectionPoints.get(i), selectionPoints.get(i + 1));
			}

			if (!selectionPoints.isEmpty())
				shaper.line(selectionPoints.get(0), selectionPoints.get(selectionPointsSize - 1));
		} else if (Starfire.SELECTION_TYPE == Starfire.SelectionType.RECTANGLE) {
			if (selectionPoints.size() > 1)
				shaper.rect(selectionPoints.get(0).x, selectionPoints.get(0).y, selectionPoints.get(1).x - selectionPoints.get(0).x, selectionPoints.get(1).y - selectionPoints.get(0).y);
		}
	}

	public void unselectAll() {
		for (Squad squad : Starfire.game.world.squads) {
			unselectSquad(squad);
		}
	}

	public void selectAll() {
		for (Squad squad : Starfire.game.world.squads) {
			selectSquad(squad);
		}
	}

	public void selectSquad(Squad squad) {
		if (squad.team != Starfire.game.playerTeam || selectedSquads.contains(squad)) return;
		selectedSquads.add(squad);
		if (!Starfire.game.ui.unselectButton.isVisible())
			Starfire.game.ui.unselectButton.setVisible(true);
	}

	public void unselectSquad(Squad squad) {
		if (squad.team != Starfire.game.playerTeam || !selectedSquads.contains(squad)) return;
		selectedSquads.remove(squad);
		if (selectedSquads.size() == 0 && Starfire.game.ui.unselectButton.isVisible())
			Starfire.game.ui.unselectButton.setVisible(false);
	}

	public void select() {
		if (selectionPoints.isEmpty()) return;

		if (Starfire.SELECTION_TYPE == Starfire.SelectionType.POLYGON) {
			Vector2[] polygonArray = selectionPoints.toArray(new Vector2[selectionPoints.size()]);

			for (Unit unit : Starfire.game.playerTeam.units) {
				if (Utils.isPointInsidePolygon(polygonArray, polygonArray.length - 1, unit.position)) {
					selectSquad(unit.squad);
				}
			}
		} else if (Starfire.SELECTION_TYPE == Starfire.SelectionType.RECTANGLE) {
			if (selectionPoints.size() <= 1) return;

			for (Unit unit : Starfire.game.playerTeam.units) {
				if (Utils.isPointInsideRectangle(selectionPoints.get(0), selectionPoints.get(1), unit.position)) {
					selectSquad(unit.squad);
				}
			}
		}

		selectionPoints.clear();
	}
}
