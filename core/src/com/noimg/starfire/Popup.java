package com.noimg.starfire;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Popup {
	private final Vector2 position;
	private final String text;
	private final BitmapFont font;
	private final Color color;
	private final float size;
	public boolean disappear;
	private float timer;

	public Popup(Vector2 position, String text, float size, Color color) {
		this.position = position;
		this.text = text;
		this.size = size > 3f ? 3f : size;
		font = Assets.Fonts.MOVAVI;
		// Создаем новый объект цвета, ибо изменение альфа-канала у главного цвена чревато ошибками
		// в рисовании других объектов
		this.color = new Color(color.r, color.g, color.b, color.a);
	}

	public void draw(SpriteBatch batch, float delta, float zoom) {
		timer += delta;
		font.getData().setScale(zoom * size);
		font.setColor(color);
		font.draw(batch, text, position.x - text.length() * zoom * 6f * size, position.y + timer * 100);
		font.getData().setScale(1);

		if (timer > 1.7f) {
			color.a -= color.a > delta ? delta : 0;
		}

		if (timer > 2.7f) {
			disappear = true;
		}

		font.setColor(Color.WHITE);
	}
}
