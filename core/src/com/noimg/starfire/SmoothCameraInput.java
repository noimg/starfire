package com.noimg.starfire;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.noimg.starfire.world.PhysicsBody;

/**
 * Реализация (которую я украл (почти)) плавной камеры, работающей и на десктопах и на телефонах
 */
public class SmoothCameraInput extends InputAdapter {
	public final OrthographicCamera cam;
	public final PhysicsBody body;
	private final Vector3 lastTouch;
	private final Vector3 tmp;
	public boolean canMove = true;
	private float smoothZoomStart;
	private float smoothZoomEnd;
	private float smoothZoomTime;
	private Vector2 smoothMoveStart;
	private Vector2 smoothMoveTarget;
	private float smoothMoveTime;
	private boolean isShaking;
	private Vector3 shakeStartPosition;
	private float shakeRadius;
	private float shakeRandomAngle;
	private Vector2 shakeAngle;
	private float startX;
	private float startY;

	public SmoothCameraInput(OrthographicCamera cam) {
		this.cam = cam;
		lastTouch = new Vector3();
		tmp = new Vector3();
		cam.zoom = 0.5f;
		startSmoothZoom(5f, 10f, 1f);
		body = new PhysicsBody(0, 0, 0, 0.94f);
	}

	public void smoothMove(Vector2 target) {
		smoothMoveStart = body.position;
		smoothMoveTarget = target;
		smoothMoveTime = 1;
	}

	private void updateSmoothMove(float delta) {
		if (smoothMoveTime <= 0) return;
		smoothMoveTime -= delta;

		body.position.interpolate(smoothMoveTarget, smoothMoveTime, Interpolation.pow2);
	}

	private void startSmoothZoom(float start, float end, float time) {
		smoothZoomTime = time;
		smoothZoomStart = start;
		smoothZoomEnd = end;
	}

	private void updateSmoothZoom(float delta) {
		if (smoothZoomTime <= 0) return;
		smoothZoomTime -= delta;

		cam.zoom = MathUtils.lerp(smoothZoomStart, smoothZoomEnd, smoothZoomTime);

		/*
		if (cam.zoom >= 5f) {
			cam.zoom = 5f;
			smoothZoomTime = 0;
		}*/
	}

	private void shake() {
		shakeRadius *= 0.9f;
		shakeRandomAngle += (180 + Starfire.random.nextInt(60));
		shakeAngle.set((float) (Math.sin(shakeRandomAngle) * shakeRadius), (float) (Math.cos(shakeRandomAngle) * shakeRadius));
		//body.position.add(shakeAngle);
		cam.translate(shakeAngle);
		cam.update();

		if (shakeRadius < 2f) {
			isShaking = false;
			cam.position.set(shakeStartPosition);
		}
	}

	public void beginShake(float radius) {
		shakeStartPosition = cam.position;
		this.shakeRadius = 100f * radius / cam.zoom;
		shakeRandomAngle = Starfire.random.nextInt(360);
		shakeAngle = new Vector2((float) (Math.sin(shakeRandomAngle) * radius), (float) (Math.cos(shakeRandomAngle) * radius));
		cam.translate(shakeAngle);
		cam.update();
		isShaking = true;
	}

	public void update(float delta) {
		body.updatePhysics();

		cam.position.x = body.position.x;
		cam.position.y = body.position.y;

		updateSmoothZoom(delta);
		updateSmoothMove(delta);
		// fixme баг со взрывами
		if (isShaking) shake();
		cam.update();
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		lastTouch.set(screenX, screenY, 0);
		startX = screenX;
		startY = screenY;

		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		if (!canMove) return false;
		//if (Math.abs(screenX - startX) < 15 || Math.abs(screenY - startY) < 15) return false;
		tmp.set(screenX, screenY, 0).sub(lastTouch).scl(-1, 1, 0).scl(cam.zoom);
		body.addDirectionForce(tmp, cam.zoom * 0.8f);
		lastTouch.set(screenX, screenY, 0);

		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		if (smoothZoomTime > 0) return false;
		float newZoom = cam.zoom * (amount > 0 ? 1.05f : 0.95f);
		if (newZoom <= Starfire.MIN_ZOOM || newZoom >= Starfire.MAX_ZOOM) return false;
		cam.zoom = newZoom;
		cam.update();
		return false;
	}
}