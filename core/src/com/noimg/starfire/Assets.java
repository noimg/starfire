package com.noimg.starfire;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.noimg.starfire.utils.Utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;

/**
 * Игровые ассеты
 */

public class Assets {
	// Стилистическая цветовая палитра
	public static class Colors {
		public static final Color WHITE 	= getColor(234, 234, 234, 1f);
		public static final Color BLACK 	= getColor(8, 	11, 17, 1f);
		public static final Color RED 		= getColor(236, 95,	103, 1f);
		public static final Color GREEN 	= getColor(153, 199, 148, 1f);
		public static final Color BLUE 		= getColor(102, 154, 204, 1f);
		public static final Color YELLOW 	= getColor(250, 200, 99, 1f);
		public static final Color MAGENTA 	= getColor(197, 148, 197, 1f);

		public static final
		Color TRANSPARENT_WHITE 			= getColor(234, 234, 234, 0.5f);

		public static Color getColor (int r, int g, int b, float a) {
			return new Color(r / 255f, g / 255f, b / 255f, a);
		}
	}

	// Шрифты
	public static class Fonts {
		public static BitmapFont MOVAVI;

		// На начальном этапе все битмап шрифты генерируются из .ttf файлов для удобства, в дальнейшем можно по дефолту
		// преобразовать все шрифты в bitmap формат и не использовать этот метод
		public static void init() {
			final String FONT_CHARS =
					"абвгдежзийклмнопрстуфхцчшщъыьэюя" +
							"abcdefghijklmnopqrstuvwxyz" +
							"АБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯ" +
							"ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
							"0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>";

			FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/movavi.ttf"));
			FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
			parameter.characters = FONT_CHARS;
			parameter.size = 20;
			Assets.Fonts.MOVAVI = generator.generateFont(parameter);
			generator.dispose();
		}
	}

	// Звуки
	public static class Sounds {
		public static final Sound SHOOT_1 		= getSound("shoot1.wav");
		public static final Sound SHOOT_2 		= getSound("shoot2.wav");
		public static final Sound SHOOT_3 		= getSound("shoot3.wav");
		public static final Sound EXPLOSION_1 	= getSound("explosion1.wav");
		public static final Sound EXPLOSION_2 	= getSound("explosion2.wav");
		public static final Sound EXPLOSION_3 	= getSound("explosion3.wav");
		public static final Sound EXPLOSION_4 	= getSound("explosion4.wav");
		public static final
		Sound BIG_SHIP_DESTROY 					= getSound("bigshipdestroy.wav");
		public static final Sound BLACK_HOLE 	= getSound("blackhole.wav");
		public static final Sound NEW_UNIT 		= getSound("newUnit.wav");

		public static Sound getSound (String name) {
			return Gdx.audio.newSound(Gdx.files.internal("sounds/" + name));
		}
		
		public static void dispose() {
			SHOOT_1.dispose();
			SHOOT_2.dispose();
			SHOOT_3.dispose();
			EXPLOSION_1.dispose();
			EXPLOSION_2.dispose();
			EXPLOSION_3.dispose();
			EXPLOSION_4.dispose();
			NEW_UNIT.dispose();
			BIG_SHIP_DESTROY.dispose();
		}
	}

	// Текстуры
	public static class Textures {
		public static final Texture ASSAULT_SHIP 	= getTexture("assaultShip.png");
		public static final Texture ARMORED_SHIP 	= getTexture("armoredShip.png");
		public static final Texture BUILDER_SHIP 	= getTexture("builderShip.png");
		public static final Texture MOTHER_SHIP 	= getTexture("motherShip.png");
		public static final Texture HOLE 			= getTexture("hole.png");
		public static final Texture STAR 			= getTexture("star.png");
		public static final
		Texture RADIAL_PROGRESS_BAR 				= getTexture("starMask.png");
		public static final Texture CARD 			= getTexture("card.png");
		public static final Texture CARD_MASK 		= getTexture("cardMask.png");
		public static final Texture EXPLOSION 		= getTexture("explosion.png");
		public static final Texture RESOURCE 		= getTexture("resourceIcon.png");
		public static final Texture STAR_SUN_MASK 	= getTexture("starSunMask.png");
		public static final Texture GRID 			= getTexture("grid.png");
		public static final Texture UNSELECT_BUTTON = getTexture("unselectButton.png");
		public static final Texture EXPLOSION_0 	= getTexture("explosion_0.png");
		public static final Texture EXPLOSION_1 	= getTexture("explosion_1.png");
		public static final Texture EXPLOSION_2 	= getTexture("explosion_2.png");
		public static final Texture EXPLOSION_3 	= getTexture("explosion_3.png");
		public static final Texture EXPLOSION_4 	= getTexture("explosion_4.png");
		public static final Texture EXPLOSION_5 	= getTexture("explosion_5.png");
		public static final Texture RESOURCE_BAR	= getTexture("resource_bar.png");

		public static Texture getTexture(String name) {
			return new Texture(Gdx.files.internal("textures/" + name));
		}

		public void dispose() {
			ASSAULT_SHIP.dispose();
			ARMORED_SHIP.dispose();
			BUILDER_SHIP.dispose();
			MOTHER_SHIP.dispose();
			HOLE.dispose();
			STAR.dispose();
			RADIAL_PROGRESS_BAR.dispose();
			CARD.dispose();
			RESOURCE.dispose();
		}
	}

	// Остальное
	public static class Misc {
		public static String[] STAR_NAMES;
		public static Skin skin;

		public static void init() {
			// Читаем список имен из .txt файла
			try {
				FileInputStream file = new FileInputStream("misc/stars.txt");
				BufferedReader br = new BufferedReader(new InputStreamReader(file));

				STAR_NAMES = new String[Utils.countLines("misc/stars.txt") + 1];

				int i = 0;
				String line;
				while ((line = br.readLine()) != null) {
					STAR_NAMES[i] = line;
					i++;
				}

				br.close();
			} catch (Exception e) {
				System.err.print("Exception occured: " + e);
			}

			skin = new Skin();
			skin.add("defaultFont", Fonts.MOVAVI);
			//skin.add ("titleFont", Fonts.TITLE_VCR_FONT);
		}
	}
}
