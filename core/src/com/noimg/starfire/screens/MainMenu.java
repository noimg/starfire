package com.noimg.starfire.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.ColorAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.world.WorldSettings;
import com.noimg.starfire.world.maps.MapEnum;

/**
 * Реализация главного меню
 */

class MainMenu implements Screen {

	private final OrthographicCamera camera;
	private final Viewport viewport;
	private final Stage stage;

	public MainMenu(final Starfire starfire) {
		camera = new OrthographicCamera();
		viewport = new ScreenViewport(camera);
		viewport.apply();

		stage = new Stage(viewport);
		Gdx.input.setInputProcessor(stage);

		Table table = new Table();
		//table.add (initTitleLabel ("STAR\nFIRE", Assets.Fonts.TITLE_VCR_FONT));
		table.setFillParent(true);
		stage.addActor(table);

		TextButton playButton = initTextButton("new game", Assets.Fonts.MOVAVI, Assets.Colors.WHITE);
		playButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				starfire.startGame(new WorldSettings(MapEnum.HEXAGON, Team.RED));
			}
		});

		TextButton settingsButton = initTextButton("settings", Assets.Fonts.MOVAVI, Assets.Colors.WHITE);
		settingsButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				//starfire.startGame (15);
			}
		});

		TextButton aboutButton = initTextButton("about", Assets.Fonts.MOVAVI, Assets.Colors.WHITE);
		aboutButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				//starfire.startGame (15);
			}
		});

		Table menu = new Table();
		menu.add(playButton).minHeight(60).minWidth(150);
		menu.row();
		menu.add(settingsButton).minHeight(60).minWidth(150);
		menu.row();
		menu.add(aboutButton).minHeight(60).minWidth(150);
		menu.setFillParent(true);
		stage.addActor(menu);
	}

	private TextButton initTextButton(String text, BitmapFont font, Color color) {
		TextButton.TextButtonStyle textButtonStyle;
		textButtonStyle = new TextButton.TextButtonStyle();
		textButtonStyle.font = font;
		textButtonStyle.fontColor = color;
		textButtonStyle.downFontColor = Assets.Colors.RED;
		textButtonStyle.pressedOffsetY = -5;

		TextButton button = new TextButton(text, textButtonStyle);
		button.setColor(Color.CLEAR);
		ColorAction ca = new ColorAction();
		ca.setEndColor(color);
		ca.setDuration(1f);

		SequenceAction actions = new SequenceAction();
		actions.addAction(Actions.delay(2f));
		actions.addAction(ca);
		button.addAction(actions);

		return button;
	}

	private Label initTitleLabel(String text, BitmapFont font) {
		Label label = initLabel(text, font, Color.CLEAR);
		ColorAction ca = new ColorAction();
		ca.setEndColor(Assets.Colors.RED);
		ca.setDuration(1f);
		MoveToAction ma = new MoveToAction();
		ma.setPosition(stage.getWidth() / 2, stage.getHeight() - 140, Align.center);
		ma.setDuration(1f);
		ma.setInterpolation(Interpolation.smooth2);

		SequenceAction actions = new SequenceAction();
		actions.addAction(Actions.delay(0.5f));
		actions.addAction(ca);
		actions.addAction(ma);
		label.addAction(actions);

		return label;
	}

	private Label initLabel(String text, BitmapFont font, Color color) {
		Label.LabelStyle titleStyle = new Label.LabelStyle();
		titleStyle.font = font;
		Label label = new Label(text, titleStyle);
		label.setColor(color);
		return label;
	}

	@Override
	public void show() {

	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(Assets.Colors.BLACK.r, Assets.Colors.BLACK.g, Assets.Colors.BLACK.b, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height, true);
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void hide() {

	}

	@Override
	public void dispose() {

	}
}
