package com.noimg.starfire;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.noimg.starfire.screens.game.GameScreen;
import com.noimg.starfire.world.WorldSettings;
import com.noimg.starfire.world.maps.MapEnum;

import java.util.Random;

/**
 * Главный класс игры. В основном содержит в себе глобальные параметры и ссылки на игровые экраны
 */
public class Starfire extends Game {

	public static final float MAX_ZOOM = 15f;
	public static final float MIN_ZOOM = 2f;
	public static final int COLLISION_CELL_SIZE = 240;
	public static final float RESOURCE_STAR_GET_TIME = 1.2f;
	public static final SelectionType SELECTION_TYPE = SelectionType.POLYGON;
	public static final float CARD_RELOAD_TIME = 2.5f;
	public static final int CARDS_IN_DECK = 200;
	public static boolean SHOW_GRID = false;
	public static boolean SHOW_RANGES = false;
	public static boolean SHOW_FORCES = false;
	public static Random random;
	public static GameScreen game;

	@Override
	public void create() {
		random = new Random();
		Assets.Fonts.init();
		Assets.Misc.init();

		WorldSettings settings = new WorldSettings(MapEnum.HEXAGON, Team.RED);
		startGame(settings);
	}

	public void startGame(WorldSettings settings) {
		game = new GameScreen(settings);
		game.initTeams();
		game.world.generate();
		setScreen(game);
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		super.render();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	public enum SelectionType {
		POLYGON, RECTANGLE
	}
}
