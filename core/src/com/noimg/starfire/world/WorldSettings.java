package com.noimg.starfire.world;

import com.noimg.starfire.Team;
import com.noimg.starfire.world.maps.Map;

/**
 * Настройки для карты
 */

public class WorldSettings {
	public final Map map;
	public final Team playerTeam;

	public WorldSettings(Map map, Team playerTeam) {
		this.map = map;
		this.playerTeam = playerTeam;
	}
}
