package com.noimg.starfire;

import com.badlogic.gdx.graphics.Texture;
import com.noimg.starfire.world.Blackhole;
import com.noimg.starfire.world.Explosion;
import com.noimg.starfire.world.Star;
import com.noimg.starfire.world.World;

import java.util.HashMap;

public enum Card {

	// <editor-fold desc="Список карт">
	// <editor-fold desc="Assault Ship">
	ASSAULT_SHIP(Type.UNIT, "Assault Ship", Assets.Textures.ASSAULT_SHIP,
			new Property<Integer>("cost", 3),

			new UpgradableProperty<Integer>("health", 4) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Integer>("damage", 2) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Integer>("speed", 1) {
				@Override
				void upgrade(int newLevel) {
					value += 1;
				}
			},

			new UpgradableProperty<Float>("deploytime", 0.825f) {
				@Override
				void upgrade(int newLevel) {
					value -= 0.01f;
				}
			},

			new UpgradableProperty<Integer>("deployamount", 15) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			}
	),
	// </editor-fold>

	// <editor-fold desc="Armored Ship">
	ARMORED_SHIP(Type.UNIT, "Armored Ship", Assets.Textures.ARMORED_SHIP,
			new Property<Integer>("cost", 3),

			new UpgradableProperty<Integer>("health", 20) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Integer>("damage", 5) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Integer>("speed", 1) {
				@Override
				void upgrade(int newLevel) {
					value += 1;
				}
			},

			new UpgradableProperty<Float>("deploytime", 0.3f) {
				@Override
				void upgrade(int newLevel) {
					value -= 0.01f;
				}
			},

			new UpgradableProperty<Integer>("deployamount", 2) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			}
	),
	// </editor-fold>

	// <editor-fold desc="Kamikadze Ship">
	KAMIKADZE_SHIP(Type.UNIT, "Kamikadze Ship", Assets.Textures.ASSAULT_SHIP,
			new Property<Integer>("cost", 3),

			new UpgradableProperty<Integer>("health", 10) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Explosion.Power>("explosionpower", Explosion.Power.NORMAL) {
				@Override
				void upgrade(int newLevel) {
					// upgrade explosion power
				}
			},

			new UpgradableProperty<Integer>("damage", 5) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			},

			new UpgradableProperty<Integer>("speed", 4) {
				@Override
				void upgrade(int newLevel) {
					value += 1;
				}
			},

			new UpgradableProperty<Float>("deploytime", 0.5f) {
				@Override
				void upgrade(int newLevel) {
					value -= 0.01f;
				}
			},

			new UpgradableProperty<Integer>("deployamount", 1) {
				@Override
				void upgrade(int newLevel) {
					value++;
				}
			}
	),
	// </editor-fold>

	// <editor-fold desc="Black Hole">
	/*
		Создает на месте союзной звезды черную дыру, засасывающую всех юнитов, включая союзных.
		Используется в качестве "самоуничтожения". Она должна долго строится и ее строительство
		должно прерываться, если звезду захватил враг
	 */
	BLACK_HOLE(Type.TRICK, "Black Hole", Assets.Textures.HOLE,
			new Action() {
				@Override
				public void action(float x, float y, World world, Star closestStar, Team team) {
					if (closestStar == null) return;
					if (closestStar.getTeam() != team) return;

					Blackhole blackhole = Blackhole.newBlackhole(closestStar,
							BLACK_HOLE.getIntProperty("power"),
							BLACK_HOLE.getFloatProperty("lifetime"),
							BLACK_HOLE.getFloatProperty("buildtime"));

					closestStar.structures.add(blackhole);
					world.blackholes.add(blackhole);
				}
			},

			new Property<Integer>("cost", 3),

			new UpgradableProperty<Float>("buildtime", 2f) {
				@Override
				void upgrade(int newLevel) {
					value -= 0.2f;
				}
			},

			new UpgradableProperty<Integer>("power", 3) {
				@Override
				void upgrade(int newLevel) {
					if (newLevel % 3 == 0) value += 1;
				}
			},

			new UpgradableProperty<Float>("lifetime", 3f) {
				@Override
				void upgrade(int newLevel) {
					value += 0.3f;
				}
			}
	),
	// </editor-fold>

	// <editor-fold desc="Assist">
	ASSIST(Type.TRICK, "Assist", Assets.Textures.ASSAULT_SHIP,
			new Action() {
				@Override
				public void action(float x, float y, World world, Star closestStar, Team team) {
					if (closestStar == null) return;
					Starfire.game.useUnitCard(ASSAULT_SHIP, closestStar, team);
				}
			},

			new Property<Integer>("cost", 3)
	),
	// </editor-fold>

	// <editor-fold desc="Antimatter">
	ANTIMATTER(Type.TRICK, "Antimatter", Assets.Textures.EXPLOSION,
			new Action() {
				@Override
				public void action(float x, float y, World world, Star closestStar, Team team) {
					Explosion.newThreadSafeExplosion(x, y, (Explosion.Power) ANTIMATTER.getProperty("power"), true);
				}
			},

			new Property<Integer>("cost", 3),

			new UpgradableProperty<Explosion.Power>("power", Explosion.Power.WEAK) {
				@Override
				void upgrade(int newLevel) {
					switch (newLevel) {
						case 2:
							value = Explosion.Power.NORMAL;
							break;
						case 3:
							value = Explosion.Power.BIG;
							break;
						case 4:
							value = Explosion.Power.HUGE;
							break;
					}
				}
			}
	);
	// </editor-fold>
	// </editor-fold>

	public final Type type;
	public final String name;
	public final Texture texture;
	public final Action action;
	private final HashMap<String, Property> properties = new HashMap<String, Property>();
	private int level = 1;

	Card(Type type, String name, Texture texture, Property... properties) {
		this(type, name, texture, null, properties);
	}

	Card(Type type, String name, Texture texture, Action action, Property... properties) {
		this.type = type;
		this.name = name;
		this.texture = texture;

		for (Property property : properties) {
			this.properties.put(property.name, property);
		}

		this.action = action;
	}

	public int getIntProperty(String name) {
		return (Integer) properties.get(name).getValue();
	}

	public float getFloatProperty(String name) {
		return (Float) properties.get(name).getValue();
	}

	public Object getProperty(String name) {
		return properties.get(name).getValue();
	}

	public void upgrade() {
		level++;

		for (Property property : properties.values()) {
			if (property instanceof UpgradableProperty) {
				((UpgradableProperty) property).upgrade(level);
			}
		}
	}

	public int getLevel() {
		return level;
	}

	public enum Type {
		UNIT, TRICK
	}

	/**
	 * Action позволяет определить уникальное действие каждой карте;
	 * используется преимущественно Уловками
	 */
	public interface Action {
		void action(float x, float y, World world, Star closestStar, Team team);
	}

	/**
	 * Property - параметр, хранит в себе имя и значение типа T
	 */
	public static class Property<T> {
		public final String name;
		protected T value;

		Property(String name, T startValue) {
			this.name = name;
			this.value = startValue;
		}

		public Object getValue() {
			return value;
		}
	}

	/**
	 * Если параметру нужно присвоить характерное ему поведение при повышении уровня карты, то
	 * используется UpgradableProperty
	 */
	public static abstract class UpgradableProperty<T> extends Property<T> {
		UpgradableProperty(String name, T startValue) {
			super(name, startValue);
		}

		abstract void upgrade(int newLevel);
	}
}