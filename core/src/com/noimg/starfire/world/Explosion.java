package com.noimg.starfire.world;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Assets;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.collisions.GameObject;
import com.noimg.starfire.units.Unit;
import com.noimg.starfire.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Explosion extends com.noimg.starfire.collisions.GameObject {

	private static final Sound[] SOUNDS = new Sound[] {Assets.Sounds.EXPLOSION_1, Assets.Sounds.EXPLOSION_2, Assets.Sounds.EXPLOSION_3, Assets.Sounds.EXPLOSION_4};
	private final Power power;
	private float timer;
	private final Animation animation;
	private final List<GameObject> completed = Collections.synchronizedList(new ArrayList<GameObject>());
	private final boolean canDealDamage;

	private Explosion(float x, float y, Power power, boolean canDealDamage) {
		super(x, y, power.power * 80);
		this.canDealDamage = canDealDamage;
		this.power = power;
		SOUNDS[Starfire.random.nextInt(SOUNDS.length)].play();
		animation = new Animation<Texture>	(0.025f,
				Assets.Textures.EXPLOSION_0,
				Assets.Textures.EXPLOSION_1,
				Assets.Textures.EXPLOSION_2,
				Assets.Textures.EXPLOSION_3,
				Assets.Textures.EXPLOSION_4,
				Assets.Textures.EXPLOSION_5
		);
	}

	public static Explosion newExplosion(float x, float y, Power power, boolean canDealDamage) {
		Explosion explosion = new Explosion(x, y, power, canDealDamage);
		Starfire.game.world.explosions.add(explosion);
		Starfire.game.world.collisionGrid.insert(explosion);
		Starfire.game.camera.beginShake(power.power);
		return explosion;
	}

	//fixme не выходит из цикла и тред фризится навечно
	public static Explosion newThreadSafeExplosion (float x, float y, Power power, boolean canDealDamage) {
		while (true) {
			if (!Starfire.game.world.collisionGrid.areAllThreadsFree()) continue;
			return newExplosion(x, y, power, canDealDamage);
		}
	}

	@Override
	public void updateCollision(GameObject nearestGameObject) {
		if (completed.contains(nearestGameObject)) return;
		completed.add(nearestGameObject);

		if (!(nearestGameObject instanceof Unit)) return;
		Unit unit = (Unit) nearestGameObject;

		float dst = unit.position.dst2(position);
		if (dst < radiusSquared) {
			unit.tempRotate(Utils.getFaceAngle(unit.position, position) + (Starfire.random.nextBoolean() ? 45 : -45));
			unit.addDirectionForce(new Vector2(unit.position.x - position.x, unit.position.y - position.y), power.power);
			if (canDealDamage && dst < radiusSquared / 10) {
				unit.hit(power.power);
			}
		}
	}

	@Override
	public void destroy() {
		Starfire.game.world.collisionGrid.remove(this);
		Starfire.game.world.explosions.remove(this);
	}

	public void draw(SpriteBatch batch, float delta) {
		timer += delta;

		if (timer > 0.025f * 6f) {
			dead = true;
		}

		batch.setColor(Color.WHITE);
		Texture currentFrame = (Texture)animation.getKeyFrame(timer);
		batch.draw(currentFrame,
				position.x - 125 - power.power * 7 - currentFrame.getWidth() / 2,
				position.y - 125 - power.power * 7 - currentFrame.getHeight() / 2,
				250 + power.power * 14 + currentFrame.getWidth(),
				250 + power.power * 14 + currentFrame.getHeight());
	}

	public enum Power {
		WEAK(5),
		NORMAL(8),
		BIG(12),
		HUGE(16);

		public final int power;

		Power(int power) {
			this.power = power;
		}
	}
}
