package com.noimg.starfire.utils;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.noimg.starfire.Starfire;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Класс для воспомогательных статических методов
 */
public class Utils {

	public static int countLines(String filename) throws IOException {
		InputStream is = new BufferedInputStream(new FileInputStream(filename));
		try {
			byte[] c = new byte[1024];
			int count = 0;
			int readChars;
			boolean empty = true;
			while ((readChars = is.read(c)) != -1) {
				empty = false;
				for (int i = 0; i < readChars; ++i) {
					if (c[i] == '\n') {
						++count;
					}
				}
			}
			return (count == 0 && !empty) ? 1 : count;
		} finally {
			is.close();
		}
	}

	public static float getFaceAngle(Vector2 target, Vector2 position) {
		float angle = (float) (Math.atan2(target.y - position.y, target.x - position.x) * (MathUtils.radiansToDegrees) - 90);
		if (angle < 0)
			angle += 360;

		return angle;
	}

	public static boolean isPointInsidePolygon(Vector2[] p, int size, Vector2 point) {
		boolean result = false;
		int j = size - 1;
		for (int i = 0; i < size; i++) {
			if ((p[i].y < point.y && p[j].y >= point.y || p[j].y < point.y && p[i].y >= point.y) &&
					(p[i].x + (point.y - p[i].y) / (p[j].y - p[i].y) * (p[j].x - p[i].x) < point.x))
				result = !result;
			j = i;
		}

		return result;
	}

	// fixme не работает с отрицательными сторонами
	public static boolean isPointInsideRectangle(Vector2 a, Vector2 b, Vector2 point) {
		return point.x > a.x && point.x < b.x && point.y > a.y && point.y < b.y;
	}

	public static Vector2 fastVectorNormalize(Vector2 vector) {
		float magnitudeSquared = vector.x * vector.x + vector.y * vector.y;
		float invSqrt = fastInverseSqrt(magnitudeSquared);
		return vector.scl(invSqrt);
	}

	private static float fastInverseSqrt(float x) {
		float xHalf = 0.5F * x;
		int temp = Float.floatToRawIntBits(x);
		temp = 0x5F3759DF - (temp >> 1);
		float newX = Float.intBitsToFloat(temp);
		newX = newX * (1.5F - xHalf * newX * newX);
		return newX;
	}

	public static float lerp(float v0, float v1, float t) {
		return (1 - t) * v0 + t * v1;
	}

	public static Vector2 pointInCircle(float x, float y, int objectsPerLayer, int place, float r) {
		float angle = place * (1f / objectsPerLayer);
		float i = x + r * (float) Math.cos(angle);
		float j = y + r * (float) Math.sin(angle);
		return new Vector2(i, j);
	}

	public static Vector2 randomPointInCircle(float x, float y, float r) {
		float angle = Starfire.random.nextInt(360);
		float i = x + r * (float) Math.cos(angle);
		float j = y + r * (float) Math.sin(angle);
		return new Vector2(i, j);
	}

	public static Vector2 screenToWorld(float x, float y) {
		Vector3 v3 = Starfire.game.camera.cam.unproject(new Vector3(x, y, 0));
		return new Vector2(v3.x, v3.y);
	}
}
