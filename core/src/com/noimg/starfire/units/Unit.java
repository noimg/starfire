package com.noimg.starfire.units;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.noimg.starfire.Card;
import com.noimg.starfire.Starfire;
import com.noimg.starfire.Team;
import com.noimg.starfire.utils.Utils;
import com.noimg.starfire.world.Explosion;
import com.noimg.starfire.collisions.GameObject;
import com.noimg.starfire.world.PhysicsBody;

import java.util.List;

/**
 * Абстрактный класс для юнитов
 */
public abstract class Unit extends PhysicsBody {

	public final Sprite sprite;
	public final Team team;
	public final Squad squad;
	private final float speed;
	private final int rangeSquared;    // Необходимо для оптимизации расчетов расстояния
	private boolean canShoot;
	private boolean isMoving;
	private float health;
	private float reloadTimer;
	private float rotation;
	private Vector2 moveTarget;
	//private Team lastHitTeam;		// Если юнит будет уничтожен, этой стороне будет начислены ресурсы
	private float patrolTimer;
	private Vector2 patrolStartPosition;
	private boolean tempRotate;
	private float tempRotateTimer;

	protected Unit(float x, float y, Squad squad) {
		super(x, y, 90 - 15, 0.9f);    //getTexture().getWidth() - 15
		this.squad = squad;
		this.team = squad.team;
		rangeSquared = getDefaultRange() * getDefaultRange();

		health = getDefaultHealth();
		speed = getDefaultSpeed();

		// Инициализируем спрайт
		Texture texture = getTexture();
		assert texture != null : "Unit texture is null";
		sprite = new Sprite(texture);
		sprite.setColor(team.color);
		//fixme из за того, что ссылка на цвет не меняется, могут
		//fixme возникнуть баги
	}

	public static Unit newUnit(Card card, float x, float y, Squad squad) {
		if (card.type != Card.Type.UNIT) return null;

		//fixme возможно как то получится избежать дубликатов?
		Unit unit = null;
		switch (card) {
			case ASSAULT_SHIP:
				unit = new AssaultShip(x, y, squad);
				break;
			case ARMORED_SHIP:
				unit = new ArmoredShip(x, y, squad);
				break;
			case KAMIKADZE_SHIP:
				unit = new KamikadzeShip(x, y, squad);
				break;
		}

		Starfire.game.world.units.add(unit);
		Starfire.game.world.collisionGrid.insert(unit);
		squad.units.add(unit);
		squad.team.units.add(unit);
		return unit;
	}

	protected abstract int getDefaultDamage();

	protected abstract int getDefaultHealth();

	protected abstract float getDefaultSpeed();

	public abstract int getDefaultRange();

	protected abstract float getDefaultReloadTime();

	public abstract Texture getTexture();

	protected abstract Sound getShootSound();

	public abstract DeathEvent getDeathEvent();

	@Override
	public void updateCollision(GameObject nearestGameObject) {
		super.updateCollision(nearestGameObject);

		nearestGameObject.collisionReady = false;
		if (!(nearestGameObject instanceof Unit)) return;
		Unit unit = (Unit) nearestGameObject;

		float dst = position.dst2(unit.position);
		// Если дистанция от юнита a до юнита b меньше, чем радиус стрельбы юнита a
		if (dst < rangeSquared) {
			// Если дистанция от юнита a до юнита b меньше, чем размер юнита
			if (solid && unit.solid && dst < radiusSquared) {
				addDirectionForce(new Vector2(
						position.x - unit.position.x,
						//position.y - unit.position.y), getForce().len() * 0.01f);
						position.y - unit.position.y), getForce().len() * 0.01f);
			}

			// Если у юнитов разные стороны (т.е. они враги)
			if (team != unit.team) {
				if (this instanceof KamikadzeShip) {
					dead = true;
				} else {
					shootAt(unit);
				}
			}
		}
	}

	public final void heal(float amount) {
		if (amount < 0) return;
		health += amount;
		if (health > getDefaultHealth()) health = getDefaultHealth();
	}

	public void hit(int damage) {
		if (damage < 0) return;
		health -= damage;
		dead = health < 1;
	}

	public void tempRotate(float angle) {
		rotation = angle;
		tempRotate = true;
	}

	/**
	 * updateReload обновляет таймер перезарядки оружия
	 *
	 * @param delta Время между кадрами
	 */
	private void updateReload(float delta) {
		if (canShoot) return;
		reloadTimer += delta;

		if (reloadTimer > getDefaultReloadTime()) {
			reloadTimer = 0;
			canShoot = true;
		}
	}

	/**
	 * updateMovement контролирует движение юнита по направлению к точке движения
	 */
	private void updateMovement(float delta) {
		updatePatrol(delta);

		if (isMoving) {
			// S = (v0^2)/(2*a)
			//float distance = (getForce ().len2 ()) / (2 * (0.001f * speed));

			if (!tempRotate)    // Немного костыль но это должно направлять юнит по направлению врага когда он рядом
				lookAt(moveTarget);

			float dst = position.dst2(moveTarget);
			if (dst < 420 * 420) {
				inertia = 0;
				solid = false;
				addDirectionForce(new Vector2(moveTarget.x - position.x, moveTarget.y - position.y).rotate(90), 5);
			} else {
				addPointForce(moveTarget, speed);
			}
		}
	}

	private void updateRotation(float delta) {
		if (tempRotate) {
			tempRotateTimer += delta;

			if (tempRotateTimer > 0.5f) {
				tempRotateTimer = 0;
				tempRotate = false;
			}
		}

		sprite.setRotation(MathUtils.lerpAngleDeg(sprite.getRotation(), rotation, 0.1f));
	}

	public void update(float delta) {
		updateReload(delta);
		updateRotation(delta);
		updateMovement(delta);
		updatePhysics();

		sprite.setCenter(position.x, position.y);
	}

	public final void move(Vector2 position) {
		inertia = 0.9f;
		solid = true;
		moveTarget = position;
		isMoving = true;
	}

	private void updatePatrol(float delta) {
		if (patrolTimer > 0) {
			patrolTimer -= delta;

			if (patrolTimer <= 0) {
				patrolTimer = 0;
				move(patrolStartPosition);
			}
		}
	}

	public final void patrol(Vector2 a, Vector2 b, float time) {
		move(a);
		patrolStartPosition = b;
		patrolTimer = time;
	}

	public void shootAt(Unit target) {
		tempLookAt(target.position);
		if (canShoot) {
			canShoot = false;
			target.hit(getDefaultDamage());
			// World.getNearestGameObjects ();
			/*
			GameObject[] nearest =
					Starfire.game.world.collisionGrid.getGameObjectsInRange(target.position.x, target.position.y, 50);

			for (GameObject go : nearest) {
				if (!(go instanceof Unit)) continue;
				Unit unit = (Unit) go;
				if (unit.team == team) continue;
				//unit.sprite.setColor(Color.CYAN);
				//unit.hit(getDefaultDamage());
			}*/

			Projectile.newProjectile(this, target);
		}
	}

	private void deathEvent () {
		DeathEvent event = getDeathEvent();
		if (event != null) {
			if (event.explosion != null) Explosion.newExplosion(position.x, position.y, event.explosion, true);
			if (event.sound != null) event.sound.play();
			if (event.customDeathEvent != null) event.customDeathEvent.event(position.x, position.y);
		}
	}

	@Override
	public void destroy() {
		if (!Starfire.game.world.collisionGrid.areAllThreadsFree()) System.out.println("thread unsafe call!");
		if (!dead) return;
		super.destroy();
		squad.units.remove(this);
		team.units.remove(this);
		Starfire.game.world.units.remove(this);
		if (squad.units.isEmpty()) {
			Starfire.game.world.squads.remove(this.squad);
		}
		Starfire.game.world.collisionGrid.remove(this);

		deathEvent();
	}

	private void lookAt(Vector2 target) {
		rotation = Utils.getFaceAngle(target, new Vector2(position.x - sprite.getWidth() / 2, position.y - sprite.getHeight() / 2));
	}

	private void tempLookAt(Vector2 target) {
		tempRotate(Utils.getFaceAngle(target, new Vector2(position.x - sprite.getWidth() / 2, position.y - sprite.getHeight() / 2)));
	}
}